import pathlib

for key in (
    "folder_sbml",
    "folder_taskdescr",
    "folder_pkn",
    "folder_ts",
    "file_sbmlList"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])


print("laaaaaaaaaa")
print(config)

gW_SBML_id, = glob_wildcards(config["folder_sbml"] / "{SBML_id}.xml")
print(config["folder_sbml"])
print(gW_SBML_id)
print(len(gW_SBML_id))
print("#########################")


rule sbmlList2foldersbml:
    # extract from the provided sbmlList, the list of sbml to processed, and put a copy of these files in config["folder_sbml"]
    input:
        sbmlList = config["file_sbmlList"]
    output:
        [
            str(config["folder_sbml"] / pathlib.Path(line.strip()).name) for line in open(config["file_sbmlList"])
        ]
    params:
        folder_sbml = config["folder_sbml"]
    run:
        os.makedirs(params.folder_sbml, exist_ok=True)
        with open(input.sbmlList, "r") as f:
            for line in f:
                print(line)
                from_path = pathlib.Path(line.strip())
                to_path = pathlib.Path(params.folder_sbml) / from_path.name
                print(from_path)
                print(to_path)
                shutil.copy(from_path, to_path)

rule all_simucopasi:
    input:
        expand(
            str(config['folder_ts'] / "{SBML_id}_ts.csv"), SBML_id=gW_SBML_id
        )

rule all_pknsif:
    input:
        expand(
            str(config['folder_pkn'] / "{SBML_id}.sif"), SBML_id=gW_SBML_id
        )


print(gW_SBML_id)
print(len(gW_SBML_id))
rule taskdescr:
    input:
        # all the sbml file:
        sbmlFiles = expand(
            str(config['folder_sbml'] / "{SBML_id}.xml"), SBML_id=gW_SBML_id
        ),
        sbmlList = config["file_sbmlList"],
        odscurationtimes = config["odscurationtimes"]
    params:
        nodesTypes = " ".join(config["nodesTypes"])
    output:
        # one yml file per sbml
        taskconfigfiles = expand(
            str(config["folder_taskdescr"] / "{SBML_id}.yaml"), SBML_id=gW_SBML_id)
    shell:
        "python3 code/make_taskdescrFiles.py --list_of_sbmlfiles_to_process {input.sbmlList} --nodesTypes {params.nodesTypes} --odscurationtimes {input.odscurationtimes} --folder_taskdescr_files {config[folder_taskdescr]}"


# The simulation of an sbml file uses the Python binding of COPASI.
# It returns a csv.
# The first column stores the time, the others columns are the metabolites' concentrations and values' values.
# Are taken into account only the agents that are not fixed. -> 15 agents for the BIOMD0000000111.xml

rule simucopasi:
    input:
        sbml = config['folder_sbml'] / "{SBML_id}.xml",
        task_descr_file = config['folder_taskdescr'] / "{SBML_id}.yaml"
    params:
        #  use a function in the params directive for it to know the wildcards.
        # https://stackoverflow.com/a/49371887
        temp = lambda w: config["folder_sbml"] / f"{w.SBML_id}_ts.csv"
    output:
        keep = config["folder_ts"] / "{SBML_id}_ts.csv"
    run:
        shell("python3 code/runsimu.py {input.sbml} {input.task_descr_file}")
        # move the csv produced at its correct location :
        print(params.temp)
        shell("cp {params.temp} {output.keep}")


# Extract influences from sbml file
# store them in a sif file
rule sbml2sif:
    input:
        sbml = config["folder_sbml"] / "{SBML_id}.xml"
    params:
        " ".join(config["nodesTypes"])
    output:
        config['folder_pkn'] / "{SBML_id}.sif"
    shell:
        "python3 code/sbml2sif.py --sbmlfilepath {input.sbml} --nodesTypes {params} --siffilepath {output}"
