import pathlib

for key in (
        "folder_ts",
        "folder_xp",
        "htmlreport_compare_nbconfig_VS_visited"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])


gW_SYSTEM_id, = glob_wildcards(
    config['folder_ts'] / "{SYSTEM_id}_ts.csv")

# to run this rule, you need to first have generated the *_binarizedtsts !!
# For this, you can use the rule `all_binarizedts`
rule compare_nbconfig_VS_visited:
    input:
        # all the bts
        expand(
            config['folder_ts'] / "{SYSTEM_id}_binarizedtsts.csv", SYSTEM_id=gW_SYSTEM_id
        )
    params:
        folder_xp = config["folder_xp"]
    output:
        directory(config["htmlreport_compare_nbconfig_VS_visited"])
    shell:
        "python3 code/htmlreport_compare_nbconfig_VS_visited.py --xprespath {params.folder_xp} --pathout {output}"
