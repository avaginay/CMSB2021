import argparse
import concurrent.futures
import glob
import itertools
import json
import logging
import multiprocessing
import pathlib
import sys
import textwrap

import snakemake
import yaml

try:
    sys.path.insert(1, 'code/')
    import xpconfighandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


def argparse_str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--filexpconfig',
                        type=str,
                        required=False,
                        help=textwrap.dedent('''\
                        yml file storing config of the experiment.
                        The things that have to be defined are:
                            - globstr_datain. default: "data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/*.xml"
                            - nb_pipeline_parallel. default:  10
                            - nb_clingo_cores. default:  5
                            - log_level. default: "INFO"
                            - optimization. default: "mae"
                            - TODO : make the list
                        Note that all these options can be override by the 'xpconfig' option
                        ''')
                        )
    parser.add_argument('--overwrite',
                        type=json.loads,
                        default={},
                        required=False,
                        help=textwrap.dedent('''\
                        Dict shape in a string to define options / override options from the filexpconfig.
                        Example :
                        '{"globstr_datain": "another/glob/", "nb_pipeline_parallel":1}'
                        ''')
                        )

    parser.add_argument("--forceall",
                        type=argparse_str2bool,
                        nargs='?',
                        const=True,
                        default=False,
                        required=False,
                        help=textwrap.dedent('''\
                        force all output files to be re-created (default False)
                        ''')
                        )
    parser.add_argument('--rules',
                        nargs="+",
                        required=True,
                        help=textwrap.dedent('''\
                        Name of the step to run.
                        '''),
                        choices=(
                            "all",
                            "taskdescr",
                            "simucopasi",
                            "simucopasi",
                            "stretchedts2binarizedts",
                            "ts2plots",
                            "sbml2sif",
                            "sbml2dot",
                            "dot2svg",
                            "breakpknsvg",
                            "sbml2lp",
                            "lp2bn",
                            "bnet2stgsif_complete",
                            "bnet2stgsif_focusedbts",
                            "stgsif2stgpng",
                            "sbmlhavingwarnings",
                            "get_curationReport",
                            "all_plots",
                            "tscsv2tsMIDAS"
                        )
                        )

    args = parser.parse_args()

    xpconfig = dict()

    if args.filexpconfig:
        xpconfig = xpconfighandler.xpconfigfile2dict(args.filexpconfig)

    print(xpconfig)
    for key, val in args.overwrite.items():
        print("key", key, "val", val)
        xpconfig[key] = val

    if not xpconfighandler.xpconfigisvalid(xpconfig):
        print(xpconfig)
        print(f"""
              some information about the xpconfig are missing.
              Please check out the list : {required}
              """)
        exit(1)

    try:
        sys.path.insert(1, 'code/')
        import logger
        logger.set_loggers(xpconfig["log_level"])
        logger = logging.getLogger('logger.BM2BNrunsnakemake')
    except ImportError:
        print("is it the correct working directory ?")
        # %pwd

    if not all([nodeType in ["SBMLspecies", "SBMLparamsnotconstant"] for nodeType in xpconfig["nodesTypes"]]):
        print(
            f"Chosen nodesTypes can only be 'SBMLspecie' and / or 'SBMLparamsnotconstant'. Has to quit, sorry")
        exit(1)

    assert(xpconfig["sm_cores"] <= multiprocessing.cpu_count())
    logger.info(
        f'{xpconfig["sm_cores"]} given to run the xp')
    logger.info(
        f"Note there is {multiprocessing.cpu_count()} cores available at total")

    pathlib.Path(xpconfig["sm_statfile"]).parents[0].mkdir(
        parents=True, exist_ok=True)
    print(args.forceall, "<-------------")
    # snakemake function returns bool (True -> the workflow execution was successful)
    success = snakemake.snakemake(
        snakefile="Snakefile",  # (str) – path to the snakefile
        cores=xpconfig["sm_cores"],
        # (list) – list of targets, e.g. rule or file names (default None)
        targets=args.rules,
        config=xpconfig,  # (dict) – override values for workflow config
        forceall=args.forceall,
        stats=xpconfig["sm_statfile"],
        printreason=True,
        printshellcmds=True,
        keepgoing=True,  # keep goind upon errors
        # report=os.path.join(
        #    config["folder_smreports"],
        #    f"report_{datetime.now().strftime('%Y%m%d-%H%M%S')}.html"
        # )  # (str) – create an HTML report for a previous run at the given path
    )

    if success:
        logger.info("Worflow has finish successfully")
    else:
        logger.error("Workflow has not finish succesfully")

    logger.info("The end")
