
xpname=$1
# xpname="20201011_xp_minMAE_minInputs"

echo "avaginay@sb-data:/data2/avaginay/bm2bn/experiment/$xpname"
rsync -azPv "avaginay@sb-data:/data2/avaginay/bm2bn/experiments/${xpname}" ./FROM_CLUSTER/${xpname}

# -a : archive mode :
#   recursive
#   preserve symbolic links (but not hard ones)
# 	preserve permission
#   preserve dates
#   preserve group
#   preserve ownership
#   preserve peripherals
# -z : compression
# -P : show progress
# -v : verbose

# Other usefull option :
# -u : update = ignore files that are more recent on the cluster
