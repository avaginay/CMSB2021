# base environement :
FROM conda/miniconda3


# update conda :
RUN conda update -n base -c defaults conda

# RUN conda config --set allow_conda_downgrades true

RUN apt-get update && apt-get install -qqy \
  python-pip \
  jq git


RUN pip install --upgrade setuptools

ADD environment_env-pipeline.yml .
ADD environment_env-caspots.yml .

ADD environement_env-CMSB2021.yml .

RUN ls .
RUN which conda
RUN python --version
RUN conda --version


# create the environment for the pipeline using the associated yaml
RUN conda env create -f environment_env-pipeline.yml --force
# force in case it already exists

# create a specific environment for caspots because it uses dinausaurus python (2.7)
# (Note we cannot just pull the docker of caspots in our wrapit because Docker in Docker is making a mess)
RUN conda env create -f environment_env-caspots.yml --force


RUN conda env create -f environement_env-CMSB2021.yml --force


###############################################################################

# To activate the conda environement in the Docker,
# we cannot just use conda init
# explanations are here :
# https://pythonspeed.com/articles/activate-conda-dockerfile/
# https://medium.com/@chadlagore/conda-environments-with-docker-82cdc9d25754


#---------- FAIL
# from https://pythonspeed.com/articles/activate-conda-dockerfile/
# Make RUN commands use the new environment:
#SHELL ["conda", "run", "-n", "env_BM2BN_createdfromDockerfile","/bin/bash", "-c"]
# The code to run when container is started:
#ENTRYPOINT ["conda", "run", "-n", "env_BM2BN_createdfromDockerfile"]
#----------

# Make RUN commands use `bash --login`:
#SHELL ["/bin/bash", "--login", "-c"]


# configure conda to use 'conda activate'. It initialize conda in bash config file.
#RUN conda init bash


# Make sure the environement is activated :
#RUN conda list -n $conda_env





#------------ FAIL :
# from https://stackoverflow.com/a/62674910
#RUN conda init bash\
#    && . ~/.bashrc \
#    && conda env create -f $env_file --name $conda_env --force\
#    && echo "ICI !"\
#    && conda activate $conda_env
# EXPLANTION :
#RUN conda init bash \ #configure conda to use 'conda activate'. It initialize conda in bash config file.
#    && . ~/.bashrc \ # restart the shell
#    && conda env create -f $env_file --name $conda_env \ # create the environment using the yaml
#    && conda activate $conda_env  # activate conda environment
#------------




#---------- FAIL
# activate the conda environement thanks to a command in the bashrc :
#RUN echo "conda activate $conda_env" > ~/.bashrc
#----------


#---------- FAIL
# Tell conda that the created env is the default env :
# ENV CONDA_DEFAULT_ENV $conda_env
#----------



#------------ FAIL :
# from https://medium.com/@chadlagore/conda-environments-with-docker-82cdc9d25754 :
# Pull the environment name out of the environment.yml
#   RUN echo "source activate $(head -1 $env_file | cut -d' ' -f2)" > ~/.bashrc
#   ENV PATH /opt/conda/envs/$(head -1 environment.yml | cut -d' ' -f2)/bin:$PATH
#RUN echo "source activate $conda_env" > ~/.bashrc
#ENV PATH /usr/local/envs/$conda_env/bin:$PATH
#------------
