# figpaper figures

# STG for BN1 and BN2 of the running example


snakemake -s CMSB2021.smk figpaper_all --cores 1  --reason -p --configfile xpconfig_CMSB2021.yml -n




###########################

rsync -azPv data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/ avaginay@sb-data:/data2/avaginay/bm2bn/data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/
rsync -azPv table_curatedBIOMD_time-curation-ts.ods avaginay@sb-data:/data2/avaginay/bm2bn/table_curatedBIOMD_time-curation-ts.ods
rsync -azPv sbmlList/* avaginay@sb-data:/data2/avaginay/bm2bn/sbmlList/


snakemake -s CMSB2021.smk sbmlList_Biomodels_curated --cores 1 --reason -p --configfile xpconfig_CMSB2021.yml
snakemake -s CMSB2021.smk htmlreport_stats_sbmlList_Biomodels_curated --cores 1  --reason -p --configfile xpconfig_CMSB2021.yml


snakemake -s CMSB2021.smk htmlreport_curation_images --cores 1  --reason -p --configfile xpconfig_CMSB2021.yml


snakemake -s CMSB2021.smk sbmlList_Biomodels_curated_withinfots --cores 1  --reason -p --configfile xpconfig_CMSB2021.yml
snakemake -s CMSB2021.smk sbmlList_Biomodels_curated_withinfotscomplete --cores 1 --reason -p --configfile xpconfig_CMSB2021.yml





# creer des sbmlList qui contienne des sbml dont le nomdre de noeuds pris en compte est inférieur à une certaine valeur :
# par tranche de 5 en 5.
python3 code/sbmlList_split-per-number-of-nodes.py --sbmlList sbmlList/sbmlList_Biomodels_curated_withinfots_runsimuOK_getpknOK  --bins 5  --outfolder sbmlList/ --nodesTypes SBMLspecies SBMLparamsnotconstant

# modify the wpconfig file accordingly

#---------
snakemake -s CMSB2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile xpconfig_CMSB2021.yml


snakemake -s CMSB2021.smk taskdescr --cores 1 --reason -p --configfile xpconfig_CMSB2021.yml

snakemake -s CMSB2021.smk all_simucopasi --cores 50 --reason -p --configfile xpconfig_CMSB2021.yml

# TODO rule to get a report to visualy compare simu created with curated knowledge and retrieve curated ts image.
# snakemake -s CMSB2021.smk ???????? --cores 1 --config dir_xp="experiments/xpname" --reason -p --configfile xpconfig_CMSB2021.yml



snakemake -s CMSB2021.smk all_pknsif --cores 50  --reason -p --configfile xpconfig_CMSB2021.yml


snakemake -s CMSB2021.smk htmlreport_stats_sbmlList_Biomodels_curated_runsimuOK_pknOK --cores 1 --reason -p --configfile xpconfig_CMSB2021.yml


snakemake -s CMSB2021.smk all_BNsidentification --cores 50  --reason -p --configfile xpconfig_CMSB2021.yml -n

# identification for a SBML in particular:
cp data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/BIOMD0000000065.xml experiments/xpname/sbmlfiles/BIOMD0000000065.xml
snakemake -s CMSB2021.smk experiments/xpname/bns/BIOMD0000000065/our/identification.done --cores 1  --reason -p --configfile xpconfig_CMSB2021.yml

clingo --opt-mode=optN --parallel-mode=1 -n 0 --stats \
  /home/wrapit/experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_M.lp\
 /home/wrapit/code/identify.lp \
  /home/wrapit/code/minimization_MAE.lp \
 /home/wrapit/experiments/xpname/lp/BIOMD0000000065/knowledge_ts-stretched.lp \
 /home/wrapit/experiments/xpname/lp/BIOMD0000000065/knowledge_ts-binarized.lp

python3 code/clingoidentification.py        --nbcores 1    \
    --stretchedtsfilepath experiments/xpname/lp/BIOMD0000000065/knowledge_ts-stretched.lp    \
    --binarizedtsfilepath experiments/xpname/lp/BIOMD0000000065/knowledge_ts-binarized.lp     \
    --outputdirpath experiments/xpname/bn/BIOMD0000000065/our       \
   --optimization mae      \
  --plotsdirpath experiments/xpname/plots/heatmaps_conflictingtimesteps/BIOMD0000000065    \
    --nbmaxgeneratedBNs 100      \
      experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_M.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_B.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_A.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_L.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_P.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_I1.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_I2.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_I3.lp experiments/xpname/lp/BIOMD0000000065/knowledge_sbmlspecies_L_e.lp
# lpfiles





snakemake -s CMSB2021.smk all_get_coverage_stgVSts --cores 5  --reason -p --configfile xpconfig_CMSB2021.yml -n




# --- Get comparative coverage boxplot
# comparative boxplots for OLA21 submission.
# one figure per system, one boxplot per identification method (for a given coverage measure and stg update scheme)

snakemake -s CMSB2021.smk all_figure_comparisoncoverage --cores 1  -p --configfile xpconfig_CMSB2021.yml  --notemp --forcerun figure_comparisoncoverage
snakemake -s CMSB2021.smk all_figure_comparisoncoverage_PyBoolNetrepo --cores 1  -p --configfile xpconfig_CMSB2021.yml  --notemp --forcerun figure_comparisoncoverage



# --- Get table number of BNs generated for all systems by all the identification methods
snakemake -s CMSB2021.smk all_number_generatedBNs --cores 1  -p --configfile xpconfig_CMSB2021.yml -n
# outputs : experiments/CMSB2021-results_merge-all_1to10/results/number_generatedBNs_table.csv



snakemake -s CMSB2021.smk all_aggregationresults --cores 1  -p --configfile xpconfig_CMSB2021.yml -n
# experiments/CMSB2021-results_merge-all_1to10/results/aggregation_resultats_table_mixed.csv
# experiments/CMSB2021-results_merge-all_1to10/results/number_generatedBNs_table.csv


snakemake -s CMSB2021.smk all_aggregation_results_image --cores 1  -p --configfile xpconfig_CMSB2021.yml -n

snakemake -s CMSB2021.smk cumulative_plot_runtime --cores 1  -p --configfile xpconfig_CMSB2021.yml -n

snakemake -s CMSB2021.smk boxplot_runtime --cores 1  -p --configfile xpconfig_CMSB2021.yml -n






# === Other examples ===


# --- Get all coverage boxplot
# They are individual boxplot drawn for a specific identification method, coverage measure, and update scheme
snakemake -s OLA2021.smk all_coverageboxplot --cores 5 --config dir_xp="experiments/xpname" --reason -p --con


# --- Get all stgsif
# snakemake -s BN-analysis.smk all_bnet2stgsif --cores 5 --config dir_xp="experiments/xpname" --reason -p --configfile xpconfig_CMSB2021.yml  --notemp -k
# --- Get all stgpng
# batch version :
for i in {1..10}
do
  snakemake -s plots.smk all_stgpng --cores 7  --reason -p --configfile xpconfig_RBoolNet-yeast.yml  --notemp --batch all_stgpng=$i/10
done
# no batch :
snakemake -s plots.smk all_stgpng --cores 7  --reason -p --configfile xpconfig_CMSB2021.yml  --notemp



#############################################################

# Running Example paper :

snakemake -s CMSB2021.smk runningexample_paperCMSB2021_sbmlList --cores 7 --reason -p --configfile xpconfig_CMSB2021_runningexample.yml  --notemp

snakemake -s CMSB2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CMSB2021.smk taskdescr --cores 1  --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CMSB2021.smk all_simucopasi --cores 50   --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CMSB2021.smk all_pknsif --cores 50  --reason -p --configfile xpconfig_CMSB2021_runningexample.yml
snakemake -s CMSB2021.smk all_BNsidentification --cores 50 --reason -p --configfile xpconfig_CMSB2021_runningexample.yml -n
snakemake -s CMSB2021.smk all_get_coverage_stgVSts --cores 1   --reason -p --configfile xpconfig_CMSB2021_runningexample.yml -n
snakemake -s CMSB2021.smk all_aggregation_results_image --cores 1   -p --configfile xpconfig_CMSB2021_runningexample.yml -n



##############################################################

#on sbnode3 caspots from 1 to 5 nodes


snakemake -s CMSB2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml
snakemake -s CMSB2021.smk taskdescr --cores 1  --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml
snakemake -s CMSB2021.smk all_simucopasi --cores 50   --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml
snakemake -s CMSB2021.smk all_pknsif --cores 50  --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml
snakemake -s CMSB2021.smk all_BNsidentification --cores 50 --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml -n
snakemake -s CMSB2021.smk all_BNsidentification --cores 50 --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml --touch
snakemake -s CMSB2021.smk all_get_coverage_stgVSts --cores 1   --reason -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml -n
snakemake -s CMSB2021.smk all_aggregation_results_image --cores 1   -p --configfile xpconfig_CMSB2021_caspots-from1to5-onsbbode3.yml -n



##############################################################

#on sbnode3 caspots from 6 to 10 nodes : --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml
#on sbnode3 caspots from 11 to 15 nodes : --configfile xpconfig_CMSB2021_caspots-from11to15-onsbbode3.yml

snakemake -s CMSB2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml
snakemake -s CMSB2021.smk taskdescr --cores 1  --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml
snakemake -s CMSB2021.smk all_simucopasi --cores 50   --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml
snakemake -s CMSB2021.smk all_pknsif --cores 50  --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml
snakemake -s CMSB2021.smk all_BNsidentification --cores 28 --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml -k
snakemake -s CMSB2021.smk all_BNsidentification --cores 50 --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml --touch
snakemake -s CMSB2021.smk all_get_coverage_stgVSts --cores 1   --reason -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml -n
snakemake -s CMSB2021.smk all_aggregation_results_image --cores 1   -p --configfile xpconfig_CMSB2021_caspots-from6to10-onsbbode3.yml -n


##############################################################

#on sbdata our from 11 to 15 nodes


snakemake -s CMSB2021.smk sbmlList2foldersbml --cores 1 --reason -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml
snakemake -s CMSB2021.smk taskdescr --cores 1  --reason -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml
snakemake -s CMSB2021.smk all_simucopasi --cores 50   --reason -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml
snakemake -s CMSB2021.smk all_pknsif --cores 50  --reason -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml
snakemake -s CMSB2021.smk all_BNsidentification --cores 28 --reason -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml -k
snakemake -s CMSB2021.smk all_get_coverage_stgVSts --cores 1   --reason -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml -n
snakemake -s CMSB2021.smk all_aggregation_results_image --cores 1   -p --configfile xpconfig_CMSB2021_our-from11to15-onsbdata.yml -n
