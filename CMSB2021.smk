
import os
import pathlib
import shutil

include: "sbml2data.smk"
# include: "data-info.smk"
include: "BN-identification.smk"
include: "BN-analysis.smk"
# include: "plots.smk"


for key in (
    "folder_pkn",
    "folder_ts",
    "folder_xp",
    "folder_bn",
    "folder_results",
    "RUNNINGEXAMPLES_folder_bn"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])

# rule runningexample_generate:
#    output:
#         bn1_bnet = config["RUNNINGEXAMPLES_folder_bn"] / \
#             "running_example" / "BN1.bnet",
#         bn2_bnet = config["RUNNINGEXAMPLES_folder_bn"] / \
#             "running_example" / "BN2.bnet"
#     run:
#         import textwrap
#         with open(output.bn1_bnet, "w") as f1:
#             f1.write(textwrap.dedent("""\
#                 ES, S
#                 P, ES
#                 S, ES
#                 E, !S
#             """))
#         with open(output.bn2_bnet, "w") as f2:
#             f2.write(textwrap.dedent("""\
#                 ES, S & E
#                 P, 1
#                 S, 0
#                 E, !S | !ES
#             """))


rule runningexample_stg:
    input:
        bnet = config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "{BNid_example}.bnet"
    output:
        sif = config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "{BNid_example}_{updatescheme}_stg.sif",
        png = config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "{BNid_example}_{updatescheme}_stg.png"
    shell:
        """
        python3 code/bnet2stgsif.py --updatescheme {wildcards.updatescheme} --bnetfile {input.bnet} --outputfile {output.sif}
        sed -i 's/-1/0/g' {output.sif}
        python3 code/stgsif2stgpng.py --stgsif {output.sif} --colorobsconfigs --drawobsseq --outpng {output.png}
        """


rule figpaper_all:
    input:
        # - stg :
        config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "BN1_synchronous_stg.png",
        config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "BN1_asynchronous_stg.png",
        config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "BN1_mixed_stg.png",
        config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "BN2_synchronous_stg.png",
        config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "BN2_asynchronous_stg.png",
        config["RUNNINGEXAMPLES_folder_bn"] / "running_example" / \
            "BN2_mixed_stg.png",

rule figpaper_stgexample:
    input:
        bnet = config["folder_bn"] / "OLA21Akutsu" / \
            "example" / "{BNid_example}.bnet"
    output:
        sif = config["folder_bn"] / "OLA21Akutsu" / "example" / \
            "{BNid_example}_{updatescheme}_stg.sif",
        png = config["folder_bn"] / "OLA21Akutsu" / "example" / \
            "{BNid_example}_{updatescheme}_stg.png"
    shell:
        """
        python3 code/bnet2stgsif.py --updatescheme {wildcards.updatescheme} --bnetfile {input.bnet} --outputfile {output.sif}
        sed -i 's/-1/0/g' {output.sif}
        python3 code/stgsif2stgpng.py --stgsif {output.sif} --colorobsconfigs --drawobsseq --outpng {output.png}
        """


rule runningexample_paperCMSB2021_sbmlList:
    output:
        "sbmlList/sbmlList_runningexample-paperCMSB2021",
    shell:
        """
        echo "data/sbml-dot-org/example_CMSB2021.xml" > {output}
        echo "data/sbml-dot-org/example_More_Detailed_Summary_of_SBML_raw.xml" >> {output}
        echo "data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/BIOMD0000000111.xml" >> {output}
        """


rule sbmlList_Biomodels_curated:
    # all the sbml from Biomodels/curated
    output:
        "sbmlList/sbmlList_Biomodels_curated"
    shell:
        "ls data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/BIOMD*.xml >  {output}"

rule htmlreport_stats_sbmlList_Biomodels_curated:
    input:
        "sbmlList/sbmlList_Biomodels_curated"
    output:
        directory("htmlreports/htmlreport_sbmlList_Biomodels_curated_stats")
    shell:
        "python3 code/htmlreport_stats-sbmlList.py --sbmlList {input} --outfolderpath {output}"

rule htmlreport_stats_sbmlList_Biomodels_curated_runsimuOK_pknOK:
    input:
        "sbmlList/sbmlList_Biomodels_curated_withinfots_runsimuOK_getpknOK"
    output:
        directory(
            "htmlreports/sbmlList_Biomodels_curated_withinfots_runsimuOK_getpknOK_stats"
        )
    shell:
        "python3 code/htmlreport_stats-sbmlList.py --sbmlList {input} --outfolderpath {output}"

rule htmlreport_curation_images:
    input:
        "sbmlList/sbmlList_Biomodels_curated"
    output:
        directory(
            "htmlreports/htmlreport_sbmlList_Biomodels_curated_curationimages")
    shell:
        """
        python3 code/htmlreport_length-curated-simulation_Biomodels.py --sbmlList {input} --outfolderpath {output}
        """

rule sbmlList_Biomodels_curated_withinfots:
    # sbml from Bomodels/curated that I found having a curation image looking like a ts
    input:
        sbmlList = "sbmlList/sbmlList_Biomodels_curated",
        curationTable = "table_curatedBIOMD_time-curation-ts.ods",
    output:
        "sbmlList/sbmlList_Biomodels_curated_withinfots"
    shell:
        "python3 code/handle_curationTable.py --sbmlList {input.sbmlList} --curationTable {input.curationTable} --outputsbmlList {output} --property 'sbmlwithcurationts'"


rule sbmlList_Biomodels_curated_withinfotscomplete:
    # sbml from Bomodels/curated that I found having a curation image looking like a ts
    # and from which i could extract timestart, timestop and unit
    input:
        sbmlList = "sbmlList/sbmlList_Biomodels_curated",
        curationTable = "table_curatedBIOMD_time-curation-ts.ods",
    output:
        "sbmlList/sbmlList_Biomodels_curated_withinfotscomplete"
    shell:
        "python3 code/handle_curationTable.py --sbmlList {input.sbmlList} --curationTable {input.curationTable} --outputsbmlList {output}  --property 'sbmlwithcurationts_complete' "

# rule run_simulation_withcurationinfots:
#     input:
#         sbmlList = "sbmlList/sbmlList_Biomodels_curated_withinfots",
#         curationTable = "table_curatedBIOMD_time-curation-ts.ods",
#     output:
#         config["folder_ts"] / "{SBML_id}_ts.csv"
#     shell:
#         # "python3 code/htmlreport_compare-ts-with-curatedts.py --xprespath {config[xp_name]}"


rule htmlreport_tswithinfoVStscurated:
    input:
        "sbmlList/sbmlList_Biomodels_curated_withinfots"
    output:
        directory(
            "htmlreports/htmlreport_sbmlList_Biomodels_curated_withinfots_tsVScurationimages")
    shell:
        "python3 code/htmlreport_compare-ts-with-curatedts.py --sbmlList {input} --outfolderpath {output}"


def aggregate_bnetfiles(wildcards):
    # input function for the rule aggregate
    print(wildcards)
    print("aggregate XXX")
    identificationdone = []

    if "our" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}" /
                                   "our" / "identification.done" for SYSTEM_id in gW_SBML_id])

    if "reveal" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}" /
                                   "reveal" / "identification.done" for SYSTEM_id in gW_SBML_id])

    if "bestfit" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}" /
                                   "bestfit" / "identification.done" for SYSTEM_id in gW_SBML_id])

    if "caspots" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}" /
                                   "caspots" / "identification.done" for SYSTEM_id in gW_SBML_id])

    # list of all the "identification.done" files" created by the identification rules
    return identificationdone


rule all_BNsidentification:
    input:
        aggregate_bnetfiles


###############################################################################
########## AGGREGATION RESULTS ################################################
###############################################################################


rule all_aggregationresults:
    input:
        config['folder_results'] / "number_generatedBNs_table.csv",
        config['folder_results'] / "aggregation_resultats_table_mixed.csv",
        # no jitter
        config['folder_results'] / \
            "aggregation_results_jitterx0jittery0.png",
        # jitter x y of 0.1
        config['folder_results'] / \
            "aggregation_results_jitterx0.1jittery0.1.png",
        # cumulative plot of runtime :


rule cumulative_plot_runtime:
    params:
        benchmarkfolder = config["folder_benchmarks"]
    output:
        pathout = "20210331_CMSB2021_cumulativeplot-cputime-hours.png"
    shell:
        """
        python3 code/benchmark_cumulative-plots.py --benchmarkfolder {params.benchmarkfolder} --pathout {output}
        """

rule boxplot_runtime:
    params:
        benchmarkfolder = config["folder_benchmarks"]
    output:
        # pathout = "20210331_CMSB2021_boxplot-cputime-hours.png"
        pathout = "20210331_CMSB2021_cumulativeplotandboxplot-cputime-hours.png"
    shell:
        # python3 code/benchmark_boxplot.py --benchmarkfolder {params.benchmarkfolder} --pathout {output}
        """
        python3 code/benchmark_cumulative-plots-and-boxplots.py --benchmarkfolder {params.benchmarkfolder} --pathout {output}
        """

rule all_aggregation_results_image:
    input:
        config['folder_results'] /
        "aggregation_results_jitterx0jittery0.png",
        config['folder_results'] /
        "aggregation_results_jitterx0.1jittery0.1.png",


rule aggregation_results_image:
    input:
        config['folder_results'] / "aggregation_resultats_table_mixed.csv"
    output:
        config['folder_results'] / \
            "aggregation_results_jitterx{jitter_x}jittery{jitter_y}.png"
    shell:
        # Note :
        # the argument "pattern" is a relicat from the code of OLA21.
        "python3 code/aggregation_results_image.py --aggregation_results_table {input} --pattern 'BIOMD' --jitter_x {wildcards.jitter_x} --jitter_y {wildcards.jitter_y} --hists True --identificationmethods caspots our --plotX median --pathout {output}"


rule aggregation_results_table:
    input:
        # all the BNs computed with all the methods for all the systems
        bns = aggregate_bnetfiles,
        # all coverage files on the wanted SYSTEM :
        # Note : in the lambda functions, the wildcards are in f-strings.
        cov_reveal = [str(p) for SYSTEM_id in gW_SBML_id for p in (
            config["folder_bn"] / f"{SYSTEM_id}" / "reveal").glob(f"*_coverage_stgVSts_mixed.csv")],
        cov_bestfit = [str(p) for SYSTEM_id in gW_SBML_id for p in (
            config["folder_bn"] / f"{SYSTEM_id}" / "bestfit").glob(f"*_coverage_stgVSts_mixed.csv")],
        cov_caspots = [str(p) for SYSTEM_id in gW_SBML_id for p in (
            config["folder_bn"] / f"{SYSTEM_id}" / "caspots").glob(f"*_coverage_stgVSts_mixed.csv")],
        # Adding the following causes an error "File/directory is a child to another output" :
        # cov_our = [str(p) for SYSTEM_id in SYSTEMS_ids for p in (
        #    config["folder_bn"] / f"{SYSTEM_id}" / "our").glob(f"*_coverage_stgVSts_mixed.csv")]
    output:
        # a table with systems in line, method in col, and values are number of BN gererated.
        # for REVEAL and BestFit, also write the number of BN before PKN filter.
        config['folder_results'] / \
            "aggregation_resultats_table_mixed.csv"
    shell:
        "python3 code/aggregation_results_table.py --bnpath {config[folder_bn]} --updatescheme mixed --pathout {output}"


rule all_number_generatedBNs:
    input:
        config['folder_results'] / "number_generatedBNs_table.csv"


rule number_generatedBNs_table:
    input:
        # all the BNs computed with all the methods for all the systems
        aggregate_bnetfiles
    output:
        # a table with systems in ligne, method in col, and values are number of BN gererated.
        # for REVEAL and BestFit, also write the number of BN before PKN filter.
        config['folder_results'] / "number_generatedBNs_table.csv"
    shell:
        # --updatescheme {config[coverage_updatescheme]}
        "python3 code/number_generatedBNs_table.py --bnpath {config[folder_bn]} --pathout {output}"
