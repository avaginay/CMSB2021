import logging
import pathlib

import graphviz

logger = logging.getLogger('logger.pknhandler')


def sifstruct2siffile(sifstruct, siffilepath):
    """
    Argument:
    ---------
    - sifstruct (list of list) :
        [ "nodeInput", valint (1 or -1), "nodeOutput" ]

    - siffilepath : str
        path of the file where to save the sif file

    """
    siffilepath = pathlib.Path(siffilepath)
    siffilepath.parents[0].mkdir(parents=True, exist_ok=True)
    with open(siffilepath, 'w') as f:
        for influence in sifstruct:
            to_write = ' '.join(map(str, influence))
            f.write(to_write + '\n')


def sifstruct2listofnodes(sifstruct):
    """
    Extract all the node mentionned in a sif structure

    Argument:
    ---------
    - sifstruct (list of list) :
        [ "nodeInput", valint (1 or -1), "nodeOutput" ]

    Returns:
    --------
    set of all the nodes mentionned in the given sif structure

    Example:
    --------
    >>> sifstruct = [["a", 1, "x"], ["b", "-1", "x"]]
    >>> sifstruct2listofnodes(sifstruct)
    {'a', 'b', 'x'}
    """
    listOfNodes = set()
    for influence in sifstruct:
        i, _, o = influence
        listOfNodes.add(i)
        listOfNodes.add(o)
    return listOfNodes


def sifstruct2linksstruct(sifstruct, listOfNodes=set()):
    """

    Argument:
    ---------
    - sifstruct (list of list) :
        [ "nodeInput", valint (1 or -1), "nodeOutput" ]
    - listOfNodes (optional set)
        set of nodes to take into account
        if none passed, it will take all the node present if the given sif structure

    Returns:
    --------
    The same information in another data structure :
    dict {y : [(x, sign), (x, sign)]} describing x--(sign)--> Y

    Examples :
    ----------

    >>> sifstruct = [["a", 1, "x"], ["b", "-1", "x"]]

    >>> listOfNodes = {"a", "b", "c", "x"}
    >>> sifstruct2linksstruct(sifstruct, listOfNodes)
    {'b': set(), 'c': set(), 'x': {('a', 1), ('b', '-1')}, 'a': set()}


    >>> sifstruct2linksstruct(sifstruct)
    {'x': {('a', 1), ('b', '-1')}, 'a': set(), 'b': set()}

    """
    listOfNodes = set(listOfNodes)
    if not listOfNodes:
        listOfNodes = sifstruct2listofnodes(sifstruct)

    links = {specie: set() for specie in listOfNodes}
    logger.debug(links)
    for from_x, sign, to_y in sifstruct:
        links[to_y].add((from_x, int(sign)))
    logger.debug(links)

    return links


def siffile2sifstruct(siffilepath, ret_listOfNodes=False):
    """
    Argument:
    ---------
    - siffilepath : (string) :
        path to the sif file
    - ret_listOfNodes (boolean optional default False)
        wether or not to return the listOfNodes associated to the siffile

    Returns:
    -------
    Tuples of sifstruct (and listOfNodes, if asked for it)
    - sifstruct : (list of list) :
        [ "nodeInput", valint (1 or -1), "nodeOutput" ]
    - listOfNodes (set)
        set of nodes mentionned in the sif file
    """
    # TODO : take ret_listOfNodes into account

    influences = []
    with open(siffilepath, 'r') as f:
        for line in f:
            influences.append(tuple(line.split()))
            # tuple : parentnode, linkvalue, childnode

    return influences


def siffile2dotfile(siffilepath, dotfilepath, listOfNodes=set()):
    """
    Arguments:
    ---------
    - siffilepath : (string) :
        path to the sif file
    - dotfilepath (str)
        path of the file where to save the dot file
    - listOfNodes (set, optional, default = empty)
        set of nodes to take into account
        if empty, it will take the list of node extracted from the sif file
    """
    sifstruct = siffile2sifstruct(siffilepath)
    if not listOfNodes:
        listOfNodes = sifstruct2listofnodes(sifstruct)
    sifstruct2dotfile(sifstruct, dotfilepath, listOfNodes)


def sifstruct2dotfile(sifstruct, dotfilepath, listOfNodes=set()):
    """
    Argument:
    ---------
    - sifstruct : (list of list) :
        [ "nodeInput", valint (1 or -1), "nodeOutput" ]

    - dotfilepath : str
        path of the file where to save the dot file


    - listOfNodes (set optional)
        set of nodes to take into account
        if none passed, it will take all the node present if the given sif structure
    """

    if not listOfNodes:
        listOfNodes = sifstruct2listofnodes(sifstruct)

    links = sifstruct2linksstruct(sifstruct, listOfNodes)

    linkstruct2dotfile(links, dotfilepath)


def linkstruct2dotfile(links, dotfilepath):
    """
    Argument:
    ---------
    - links : a links structure. Shaped like this :
        dict {y : [(x, sign), (x, sign)]} describing x--(sign)--> Y

    - dotfilepath : str
        path of the file where to save the dot file

    """
    dot = graphviz.Digraph(name="out", filename=".", format="gv")
    dot.graph_attr.update(ordering='out')

    for node in links:
        logger.debug(node)
        dot.node(node, color="black")

    for nodechild, relations in links.items():
        logger.debug(nodechild)
        for (nodeparent, value) in relations:
            logger.debug(nodeparent, value)
            dot.edge(nodeparent, nodechild,
                     color="green" if int(value) == 1 else "red")

    dotfilepath = pathlib.Path(dotfilepath)
    dotfilepath.parents[0].mkdir(parents=True, exist_ok=True)
    with open(dotfilepath, "w") as fdot:
        fdot.write(dot.source)
