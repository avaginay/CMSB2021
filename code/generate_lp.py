# %% begin
# use hydrogen cell :
# hydrogen:run-cell-and-move-down (Alt+Shift+Enter)
# cell-navigation:next-cell (Alt+j)
# cell-navigation:previous-cell (Alt+k)

# %% Various imports

import argparse
import itertools
import logging
import math
import os
import sys
import textwrap
from collections import defaultdict
from datetime import datetime

import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import logger
    import tshandler
    from powerset2asp import get_powerset, is_contradictive
    logger.set_loggers()
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


logger = logging.getLogger('logger.sbml2lp')


def argparse_str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        Produces PKN + observation encoded in ASP for each agent in the provided sbml file.
        Usage :
            python3 sbml2pknslp.py resusimupath outputdirectory
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    # script name, res-simu.txt pknlpoutfilepath, plot option, optimization option, dirpath where to store frequency of values from the streatched ts
    parser.add_argument('--sif_fname',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Sif file where the interactions are stored.
                        ''')
                        )
    parser.add_argument('--res_simu_fname',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        csv file storing the result of the copasi simulation of the sbml file  being processed
                        ''')
                        )
    parser.add_argument('--out_dirpath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the folder where lp files will be stored
                        ''')
                        )
    parser.add_argument('--optimization_option',
                        type=str,
                        choices=("mae", "unexplainedbinary"),
                        required=True,
                        help=textwrap.dedent('''\
                        if optimization_option="mae" :
                          lp program will have access to *ALL the timestep* of the simulation
                          + all the timestep of the binarization
                          ASP program will have to minimize the MAE betwwen :
                           -  obs ts (stretched between -100 and 100)
                           - binarized ts (-100 representing 0 and 100 representing 1)
                          MAE correspond to the expected value of the absolute error loss.
                          Its scale is the same as simultation data.
                          It is more interpretable than RMSE

                        if optimization_option="unexplainedbinary" :
                          lp program will be given only the binarized ts.
                          (potentielly deduplicated if no contradiction has been spoted).
                          And ASP will have to minimize the number of timestep where
                          the parents configuration observed at time T-1 as been observed to be the cube
                          but the child has not been activated at time T.
                        ''')
                        )
    parser.add_argument("--globally",
                        type=argparse_str2bool,
                        nargs='?',
                        const=True,
                        required=True,
                        help=textwrap.dedent('''\
                            Whether to use the same strtching for all the components or not.
                            Default = False -> the stretching is done component wise, resulting in having a different binarization treshold for all the component in the binarisation step.
                        ''')
                        )

    parser.add_argument('--max_inputcombi',
                        type=int,
                        required=False,
                        default=None,
                        help=textwrap.dedent('''\
                            From how many input possibilities the solver should give up ?
                            Remember : n input possibilities -> 2^n possible functions.
                            2^ 10 = 1024
                            2^ 20 = 1 048 576
                            2^ 30 = 1 073 741 824
                            2^100 = 1 267 650 600 228 229 401 496 703 205 376
                            ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()

    logger.info(f"Outputs in '{args.out_dirpath}'")

    os.makedirs(args.out_dirpath, exist_ok=True)

    # ----------
    # Open the ts to retrieve the list of nodes = the header of the ts

    timecourse = tshandler.tsfile2pddf(args.res_simu_fname)
    # timecourse = tshandler.tsfile2pddf('data/test/BIOMD0000000111_res-simu.csv')

    # model = sbmlparser.sbmlfile2libsbmlmodel(args.sbmlfilepath)
    # listOfNodes = sbmlparser.getListOfNodes(model, args.nodesTypes, safe=True)
    listOfNodes = timecourse.columns.values
    logger.info(f"List of nodes : {listOfNodes}")

    # ----------
    # %% Retrieve influences -> read the sif file into the 'influences' structure
    # = list of tuples
    # nodeY < relationship type : +1 ou -1 > nodeX
    # nodeY -> nodeX
    influences = []
    with open(args.sif_fname, 'r') as siff:
        for line in siff:
            influences.append((line.strip().split()))

    logger.info("Influences have been retrieved")
    logger.debug(influences)

    # %% New data structure to store links as
    # dict {y : [(x, sign), (x, sign)]} describing x--(sign)--> Y
    # Note that we set the keys to be sure to take all the nodes into account.
    # Even those that are not implicated in an influence
    links = {specie: set() for specie in listOfNodes}
    logger.debug(links)
    for influence in influences:
        from_x, sign, to_y = influence
        links[to_y].add((from_x, sign))
    logger.info("Influences are now stored in another data structure")
    logger.debug(links)

    # %% Print some stat about the PKN :
    # agent    #parents     #links     autoloop       link details
    logger.info("Stats about the PKN")
    nbparents = []
    autoloop = []
    for child in links:
        speciparent_namelist = list(
            set(specieparent for (specieparent, _) in links[child]))
        nbparents.append(len(speciparent_namelist))
        autoloop.append("yes" if child in speciparent_namelist else "nop")

    # links["cdc13t"]

    df = pd.DataFrame(
        {
            'agent': [child for child in links.keys()],
            '#parents': nbparents,
            '#links': [len(links[child]) for child in links.keys()],
            'autoloop': autoloop,
            'details': [links[child] for child in links.keys()]
        }
    )

    logger.info(df)

    logger.debug(
        f"nombre max de parent dans ce system : max(nbparents) = {max(nbparents)}")
    logger.debug(
        "2**2**max(nbparents) possible functions of nbparents inputs : 2**2**nbparents ")

    # pickle.dump(links, open( "links.p", "wb" ) )

    # -----------
    # Prepare the time serie streatcing its values -100 and 100
    # Prepare the binarized ts applying threasold of 0 on the stretched ts.
    #   binarized ts is filled of -1 (encoding 0) and 1

    # Since ASP cannot use float, we map all the values as integer between -100 (representing 0) and 100 (representing 1)

    # streatching and intergerization
    timecourse = tshandler.ts2stretchedts(
        timecourse, minstretched=-100, maxstretched=100, globally=args.globally)

    logger.info("ts stretched")
    logger.info(timecourse.head())

    # if mae option, write all the timecourse in knowledge_ts-stretched.lp file :
    if args.optimization_option == "mae":
        out_fpath = os.path.join(
            args.out_dirpath, f"knowledge_ts-stretched.lp")
        with open(out_fpath, "w") as fout:
            fout.write(f"% lp file storing knowledge on stretched ts \n")
            fout.write("% file generated automatically by sbml2pknlp.py\n")
            fout.write(f"% {datetime.now()}\n\n\n")

            fout.write("\n\n\n")
            fout.write("%=======================\n")
            fout.write("% obs(T, N, V)\n")
            fout.write("% value V of the node N at timestep T.\n")

            fout.write("\n")
            for T, (_, row) in enumerate(timecourse.iterrows()):
                for N in timecourse.columns:
                    V = row[N]
                    # print(f"obs({T}, {N}, {V}).")
                    fout.write(f"obs({T}, {N}, {V}). ")
                fout.write("\n")
            fout.write("\n\n\n")

    # -----
    # binarization
    # Since the values are all map between -100 and 100, the binarization threasold is 0
    timecourse_binarized = tshandler.binarization(timecourse, treasold=0)
    logger.info("ts binarized to values -1 and 1")

    # ---
    # if mae option, write all the binarized ts in knowledge_ts-binarized.lp file :
    if args.optimization_option == "mae":
        out_fpath = os.path.join(
            args.out_dirpath, f"knowledge_ts-binarized.lp")
        with open(out_fpath, "w") as fout:
            fout.write(f"% lp file storing knowledge on binarized ts \n")
            fout.write("% file generated automatically by sbml2pknlp.py\n")
            fout.write(f"% {datetime.now()}\n\n\n")

            fout.write("\n\n\n")
            fout.write("%=======================\n")
            fout.write("% obsBinarized(T, N, V)\n")
            fout.write(
                "% binarized value V (either 1 or -1) of the node N at timestep T.\n")

            fout.write("\n")
            for T, (_, row) in enumerate(timecourse_binarized.iterrows()):
                for N in timecourse_binarized.columns:
                    V = row[N]
                    # print(f"obsBinarized({T}, {N}, {V}).")
                    fout.write(f"obsBinarized({T}, {N}, {V}). ")
                fout.write("\n")
            fout.write("\n\n\n")

    # -----------
    # %% Output the lp file for each node

    # if args.optimization_option == "mae", lp file stores
    #   - the pkn
    #   - the ts for the speciechild only (named 'x')
    #   - the binarized ts for the speciechild only (named 'x')
    #   -  TODO : only store the name of the speciechild and change lp constraints to use this name directly...
    # if args.optimization_option == "unexplainedbinary", lp file stores
    #   - the pkn
    #   - the binarized ts for parents + speciechild (called x)
    #       *potentially deduplicated*

    # speciechild = "cdc13t"
    # speciechild = "k25"
    # speciechild = "l"
    # speciechild = "a"
    # speciechild = "l_e"
    for speciechild in links.keys():

        out_fpath = os.path.join(
            args.out_dirpath, f"knowledge_{speciechild}.lp")

        logger.info(f"----------\nProcessing knowledge for {speciechild}")

        with open(out_fpath, "w") as fout:

            fout.write(f"% lp data input for {speciechild}\n")
            fout.write("% file generated automatically by sbml2pknlp.py\n")
            fout.write(f"% {datetime.now()}\n\n\n")

            # If the speciechild has no parent, just write a satisfiable lp file
            # for speciparent_namelist :  use a set to remove dupies
            #                             use a list to ensure the order is respected
            speciparent_namelist = list(
                set(specieparent for (specieparent, _) in links[speciechild]))
            if len(speciparent_namelist) == 0:

                # speciechild has no parents
                # -> is constant
                # its value is the most common value observed in the binarized ts.
                # 1 (encode presence) or -1 (encodes abscence)
                v = timecourse_binarized[speciechild].value_counts().index[0]

                logger.info(
                    f"{speciechild} has no associated parent. We directly define that it is a constant of value {v} (being the most common observation in the binarized ts)")
                fout.write(f"% {speciechild} has no associated parent\n")
                fout.write("% Constant clause is the only possibilty\n")
                fout.write("clauseID(0).\n")
                fout.write(f"constant({v}).\n")
                fout.write("% We will write nothing else\n")
                continue

            # write pkn :
            fout.write("%-----------------------------------------%\n")
            fout.write("% pkn(parent, child, value)\n")
            fout.write(f"%child is denoted by x. Here, it is {speciechild}\n")
            fout.write("% 1 : positive interaction\n")
            fout.write("% -1 : negative interaction\n")
            logger.debug(
                f"{speciechild} has {len(links[speciechild])} interactions")
            for (specieparent, val) in links[speciechild]:
                fout.write(f'pkn({specieparent}, x, {val}).\n')

            # write the possible clause
            fout.write("\n\n\n")
            fout.write("%-----------------------------------------%\n")
            fout.write(
                "% this part encode the possible clauses that would form the boolean expression\n")
            fout.write("% that we will then generate\n")

            # atom are parents id appearing both :
            # in uppercase (if positively present)
            # and in lowercase (if negatively present)
            atoms_posneg = list(zip(
                [specieparent.upper() for specieparent in speciparent_namelist],
                [specieparent.lower() for specieparent in speciparent_namelist]
            ))
            # flattering :
            atoms_posneg_unzip = list(itertools.chain(*atoms_posneg))
            atoms_posneg_unzip

            ps = get_powerset(atoms_posneg_unzip, save_mem=True)

            combiID = 0
            for combi in ps:
                # combi = set(combi) # XXX it is really necessary ?
                # print(combiID, combi)

                # remove contradictive :
                if is_contradictive(combi):
                    continue

                combiID += 1  # increment the count only if the combi will be taken into account

                if args.max_inputcombi and combiID > args.max_inputcombi:
                    toomanyinputcombi = True
                    fout.write("%-----------------------------------------%\n")
                    fout.write(
                        f"% The node has too many parents (> {args.max_inputcombi})\n")
                    fout.write(
                        "% even after having removed the contradictory.\n")
                    fout.write("% Hence we allow only the constant%\n")
                    fout.write("clauseID(0).\n")
                    break

            if not toomanyinputcombi:
                combiID = 0
                for combi in ps:
                    # combi = set(combi) # XXX it is really necessary ?
                    # print(combiID, combi)

                    # remove contradictive :
                    if is_contradictive(combi):
                        continue

                    fout.write("\n")
                    fout.write(f"nbInput({combiID}, {len(combi)}).\n")
                    # print(f"nbInput({combiID}, {len(combi)}).\n")
                    for a in speciparent_namelist:
                        if a.upper() in combi:
                            val = 1
                        elif a.lower() in combi:
                            val = -1
                        else:
                            val = 0
                        fout.write(
                            f"possibleClause({combiID}, {a}, {val}).\n")

                    combiID += 1  # increment the count only if the combi will be taken into account

                # Generate the clause combinaison :
                # note that combiID is now equal to the last possibleClauseID that has been generated
                fout.write("\n\n\n")
                fout.write("%-----------------------------------------%\n")
                fout.write("% Generates the combinations :\n")
                fout.write(
                    f"1{{ clauseID(0..{combiID-1}) }}. % que certains clause vont marcher. Au moins 1, en tout cas.)")
                fout.write("\n")
                fout.write("%-----------------------------------------%\n")

            # -----
            # Generate the observations :

            # if mae option, write the ts stretched and binarized ts for the speciechild
            if args.optimization_option == "mae":

                fout.write("\n\n\n")
                fout.write("% obs(T, x, V)\n")
                fout.write(
                    "% value V of the speciechild node at timestep T.\n")

                # ts has been prepared before the begining of the loop.
                # we just have to subset only the columns we need -> the column about speciechild.
                timecourse.head()

                usecol = speciechild
                if usecol not in timecourse.columns:
                    logger.warning(
                        f"col is not available in the ts, namely {usecol}")

                timecourse_subset = timecourse[usecol]
                timecourse_subset.rename("x", inplace=True)
                timecourse_subset.head()

                fout.write("\n")
                for T, (_, V) in enumerate(timecourse_subset.items()):
                    # print(f"obs({T}, x, {V}).")
                    fout.write(f"obs({T}, x, {V}). ")
                    fout.write("\n")
                fout.write("\n\n\n")

                # ---
                # binarized ts has been prepared before the begining of the loop.
                # we just have to subset only the column we need -> the speciechild
                timecourse_binarized.head()
                timecourse_binarized_subset = timecourse_binarized[usecol]
                timecourse_binarized_subset.rename("x", inplace=True)
                timecourse_binarized_subset.head()

                fout.write("\n")
                for T, (_, V) in enumerate(timecourse_binarized_subset.items()):
                    # print(f"obsBinarized({T}, x, {V}).")
                    fout.write(f"obsBinarized({T}, x, {V}). ")
                    fout.write("\n")
                fout.write("\n\n\n")

            elif args.optimization_option == "unexplainedbinary":
                # write the binarized ts of the parents + speciechild
                # no need of the streched ts

                # binarized ts has been prepared before the begining of the loop.
                # we just have to subset only the column we need
                # these columns are :
                # the parents and the one of the node x.

                usecols = []
                usecols.extend(speciparent_namelist)
                usecols.append(speciechild)

                # import numpy as np
                # usecols = np.unique(usecols)
                usecols = [
                    col for col in usecols if col in timecourse_binarized.columns]
                cols_not_available = [
                    col for col in usecols if col not in timecourse_binarized.columns]
                if cols_not_available:
                    logger.warning(
                        f"some col are not available in the ts, namely {cols_not_available}")

                timecourse_binarized_subset = timecourse_binarized[usecols]
                # the last column is the speciedchild that we rename 'x'
                timecourse_binarized_subset = timecourse_binarized_subset.set_axis(
                    [*timecourse_binarized_subset.columns[:-1], 'x'], axis=1, inplace=False)
                timecourse_binarized_subset.head()

                # check if inconsistencies in the deduplicated time serie :
                # if there is at least one inconsistency spotted in the deduplicated ts,
                # we keep all the time serie,
                # and ASP will have to minimize the number of time step it does not explain
                # otherwise, we use directly the deduplicated ts in ASP
                # so the problem will run faster.

                # why do we first deduplicate ?
                # because in such a situation :
                # 01 0
                # 01 0
                # __ 1
                # python will spot a conflict for 01 that gave both 0 and 1
                # rather than if we first deduplicate, their is no such thing.

                # Drop consecutive duplicates
                oldlen = len(timecourse_binarized_subset)
                # don't use :
                # timecourse_subset.drop_duplicates(inplace=True)
                # timecourse_subset.plot()
                # but this crazy thing :
                timecourse_binarized_subset_deduplicated = timecourse_binarized_subset.loc[(
                    timecourse_binarized_subset.shift() != timecourse_binarized_subset).any(axis=1)]
                timecourse_binarized_subset_deduplicated.head()

                # if ts2plots.istoplot("all", args.plots):
                #    ts2plots.plot_timecourse_binarized_subset_deduplicated(
                #        timecourse_binarized_subset_deduplicated, args.outplots_dirpath)

                newlen = len(timecourse_binarized_subset_deduplicated)

                timecourse_binarized_subset_deduplicated

                inconsistency_check = defaultdict(set)
                for (i, valparents), valx in zip(
                    timecourse_binarized_subset_deduplicated.loc[:,
                                                                 timecourse_binarized_subset_deduplicated.columns != 'x'
                                                                 ].iterrows(),
                        timecourse_binarized_subset_deduplicated["x"].shift(periods=-1, axis='rows')):

                    logger.debug("---", i)
                    logger.debug(valparents, valx)

                    # key is a string representing parent state
                    key = ''.join(valparents.astype(str).values)
                    logger.debug(key)
                    if not math.isnan(valx):
                        inconsistency_check[key].add(valx)

                inconsistency_check
                inconsistency_flag = False
                inconsistency_count = 0
                for k, v in inconsistency_check.items():
                    if len(v) > 1:
                        inconsistency_flag = True
                        inconsistency_count += 1
                        logger.warning(
                            f"there is an inconsistency for the input {k} that might cause an unsat... :/ ")
                        logger.warning(
                            "so we do not remove the consecutive duplicate from the ts")
                        logger.warning(
                            "and ASP will have to deal with optimization")

                if not inconsistency_flag:
                    logger.info(
                        "no inconsistency found in the deduplicated ts")
                    logger.info(
                        "so we now use the deduplicated ts rather than the complete ts")
                    logger.info("so the ASP problem to be quicker to solve")
                    timecourse_binarized_subset = timecourse_binarized_subset_deduplicated
                    logger.info(
                        f"{oldlen-newlen} lines have been remove for the timeserie because duplicated once binarized")

                del timecourse_binarized_subset_deduplicated

                fout.write("\n\n\n")
                fout.write("% obsBinarized(T, N, V)\n")
                fout.write(
                    "% binarized value V (-1 / 1) of the node N at timestep T.\n")

                for T, (_, row) in enumerate(timecourse_binarized_subset.iterrows()):
                    for N in timecourse_binarized_subset.columns:
                        V = row[N]
                        # print(f"obs({T}, {N}, {V}).")
                        fout.write(f"obsBinarized({T}, {N}, {V}). ")
                    fout.write("\n")
                fout.write("\n\n\n")

            else:
                logger.error(
                    f"Error optimization_option unknown :{args.optimization_option}")

        logger.info(
            f"The resulting lp file describing the intergation graph of {speciechild} is in {out_fpath}")

    logger.info("End sbml2lp")
    # %% end
