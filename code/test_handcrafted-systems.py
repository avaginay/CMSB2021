# python3 -m pytest code/test_handcrafted-systems.py -v -s
# use -s for pytest to print my prints

"""
A test does not run ? Check this out:

- check that the directory is globed in the potentialTestsFolders
- check that is_handcraftedtest_folder(path) return True
    In should if it contains a pkn.sif and a ts.csv files.
- check that all the knowledge fils have been found.
    In particular knowledge about node that should be named "knowledge_node<ID>.lp"
                                                             --------------
    (because the test assumes that the nodes are called node<ID>)

"""

# parametrized tests inspired from https://stackoverflow.com/a/56813896

import difflib
import filecmp
import pathlib
import shutil
import subprocess

import pytest

from pytest_html import extras


def test_extra(extra):
    extra.append(extras.text("some string"))


# Each handcrafted system is a couple ts.csv + pkn.sif
# that are stored in their own folder in `data/handcrafted-systems/<path/to/test>`
# we retrieve these folders
BASEPATH_IN = pathlib.Path(
    '.', 'data', 'handcrafted-systems').resolve()

potentialTestsFolders = BASEPATH_IN.glob("*/**/")

# potentialTestsFolders = list(potentialTestsFolders)
# potentialTestsFolders
# potentialTestFolder = potentialTestsFolders[0]

failingTests = [
    "nodeA/A&BinPKN/4_Arandomlyincreasing-BfollowsAwitherror-XfollowsAwitherror",
    "nodeA/A&BinPKN/3_Arandomlyincreasing-BisnoisifiedA-XfollowsA",
    "nodeA/A&BinPKN/2_Arandomlyincreasing-Brandomlyincreasing-XfollowsA",
    "False/2_Arandom-Xrandom-uncorrelated",
    "False/3_Arandom_XfollowsA",
    "False/4_Aincreases-XistheoppositeofA",
    "Akutsu"
]


def is_handcraftedtest_folder(path):
    """
    Decides if a given dir is a test folder.
    (yes if it contains a pkn.sif and a ts.csv)

    Argument :
    ----------
    path : pathlib path

    Returns :
    ---------
    True / False depending of if the directory is considered to be a test folder
    """
    pathsif = path / "pkn.sif"
    pathts = path / "ts.csv"
    if pathsif.is_file() and pathts.is_file():
        print(f"pkn.sif and ts.csv have been found in {path}")
        return True
    return False


testfolders = [
    folder for folder in potentialTestsFolders if is_handcraftedtest_folder(folder)]
print(testfolders)

# Since potentialTestFolder has been constructed using BASEPATH:
assert(all(str(folder).startswith(str(BASEPATH_IN))
           for folder in testfolders))
# we can then "substract" potentialTestFolder from BASEPATH to get testname:
testnames = [str(folder).replace(str(BASEPATH_IN), '')
             for folder in testfolders]
print(testnames)
# we remove the '/' at the begining of the string so it is not considered as a path starting from root.
testnames = [tn.lstrip('/') for tn in testnames]
print(testnames)
# testnames = [pathlib.Path(tn) for tn in testnames]
# print(testnames)


BASEPATH_OUT = pathlib.Path(".", "experiments", "handcraftedtest").resolve()
if BASEPATH_OUT.exists() and BASEPATH_OUT.is_dir():
    shutil.rmtree(BASEPATH_OUT)
BASEPATH_OUT.mkdir(parents=True, exist_ok=True)


# Parametrized test with explicit ids :
# https://hackebrot.github.io/pytest-tricks/param_id_func/
@pytest.mark.parametrize('testname, testfolder', zip(testnames, testfolders), ids=testnames)
def test_handcraftedsystem(testname, testfolder):
    """
    Test an handcrafted system returned the exepted bnet(s)
    Expected in the sense of what I thought it would return.
    But these test might fail not because of a bug in the code but because it really does what I asked for but what I asked for might not be what I thought it would do.
    """

    assert(is_handcraftedtest_folder(testfolder))

    if testname in failingTests:
        # https://github.com/pytest-dev/pytest/issues/124#issuecomment-112199926
        pytest.xfail("Various reasons")

    print(f"{testfolder} has been considered to be a test directory")
    print(f"Test name = {testname}")

    # -----
    # create lp file from the provided sif and csv
    cmd = ["python3",  "code/sbml2lp.py",
           "--sif_fname", f"{BASEPATH_IN/testname/'pkn.sif'}",
           "--res_simu_fname", f"{BASEPATH_IN/testname/'ts.csv'}",
           "--out_dirpath", f"{BASEPATH_OUT/testname/'lp'}",
           "--optimization_option", "mae",
           "--plot_option", "False"
           ]
    subprocess.run(cmd)

    # -----
    # run clingo identification

    # retrieve knowledge files :
    kfiles = list((BASEPATH_OUT / testname /
                   "lp").glob("knowledge_node*.lp"))

    cmd = [
        "python3", "code/clingoidentification.py",
        "--stretchedtsfilepath", f"{BASEPATH_OUT / testname / 'lp/knowledge_ts-stretched.lp'}",
        "--binarizedtsfilepath", f"{BASEPATH_OUT / testname / 'lp/knowledge_ts-binarized.lp'}",
        "--outputdirpath", f"{BASEPATH_OUT / testname / 'bnets/'}",
        "--nbcores", "1",
        "--optimization", "mae",
        "--plotsdirpath", f"{BASEPATH_OUT / testname / 'plots/'}"
    ] + kfiles

    subprocess.run(cmd)

    # -----
    # test if it generated expected results
    left_path = BASEPATH_IN / testname / 'bnets'
    right_path = BASEPATH_OUT / testname / 'bnets'

    # files to ignore
    # https://stackoverflow.com/a/59203768
    ignore_list = []
    patterns_to_ignore = ['*~']

    for pattern in patterns_to_ignore:
        ignore_left = [expanded.name for expanded in left_path.glob(pattern)]
        ignore_right = [expanded.name for expanded in right_path.glob(pattern)]
        ignore_list.extend(ignore_left)
        ignore_list.extend(ignore_right)

    print(f"{ignore_list} are files ignored from the comparison")

    comparison = filecmp.dircmp(left_path, right_path, ignore=ignore_list)
    # /!\ dircmp function compares files by doing shallow comparisons
    # -> only takes os.stats into account !

    print("Report full closure : ")
    print(comparison.report_full_closure())
    print("-----")

    # all the files from the expected should have be generated
    # no specific file (only section) should have be generated but not expected
    # no specific file (only section) should have be expected but not generated
    assert(not comparison.left_only)
    assert(not comparison.right_only)

    # plus, the content of the common files should be the same.
    # (Note that dircmp only did a swallow comparison)
    # comparison.common_files = Files in both a and b
    matches, mismatches, errors = filecmp.cmpfiles(left_path, right_path,
                                                   common=comparison.common_files,
                                                   shallow=False
                                                   )
    print(errors)
    assert(not errors)

    print(mismatches)

    for mismatch in mismatches:
        with open(left_path / mismatch, 'r') as l, open(right_path / mismatch, 'r') as r:
            diff = difflib.unified_diff(
                l.readlines(),
                r.readlines(),
                fromfile='l',
                tofile='r',
            )
        for line in diff:
            print(line)

    assert(not mismatches)

    # Other filters :
    #   Files which are in both a and b, whose contents differ according to the class’s file comparison operator.
    #       comparison.diff_files
    #   Files which are identical in both a and b, using the class’s file comparison operator.
    #       comparison.same_files
