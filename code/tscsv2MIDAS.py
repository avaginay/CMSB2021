import argparse
import sys
import textwrap

import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


if __name__ == "__main__":

    # %% parse arguments
    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--tscsv',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        time serie in csv.
                        Columns name = specie name
                        Rowname = timestep index
                        ''')
                        )

    parser.add_argument('--outputfilepath',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        path where to save the generated MIDAS file
                        ''')
                        )

    # %% Retrieve arguments
    args = parser.parse_args()
    # sys.argv = ["--tscsv", "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/xpname/ts/BIOMD0000000041_binarizedtsts.csv", "--outputfile", "test_out_MIDAS_file.csv"]
    # args = parser.parse_args(sys.argv)

    args

    df = tshandler.tsfile2pddf(args.tscsv)

    # Timestep have to be int
    # to avoid following error message from caspots :
    # WARNING: Experiment XXX with clamping {XXX} has only one data point (at time=0)!
    # probably because it does :
    #   times = list(set(map(int,row.filter(regex='^DA:').values)))
    # in here : https://github.com/pauleve/caspots/blob/4bf07f1e24eadfd0e976adb9de3bb35e0c6d8d21/caspots/dataset.py#L133
    # to retrieve the times and it then uses the time to label the Experiments in `exp_of_clamps` and to add obs in `add_obs`.
    # In any case :
    # no example of file they gave had floting point DA value...
    df.reset_index(drop=True, inplace=True)
    # drop = True -> do not put index as a df col.
    # (otherwise it will be considered as a specie in the following code... )

    # description of the MIDAS file format :
    # https://saezlab.github.io/cellnopt/6_CNODocs/
    # in paragraph 2.2
    # CSV files (comma separated)
    df_out = pd.DataFrame()

    for colname in df:
        DA_colname = "DA:" + colname
        DV_colname = "DV:" + colname

        df_out[DA_colname] = df.index
        df_out[DV_colname] = df[colname].values

    # df_out['ID:exp'] = 'simulatedODE'
    # df_out['TR:none'] = 1

    df_out.to_csv(
        path_or_buf=args.outputfilepath, index=False)

    for cn in df_out:
        print(cn)
