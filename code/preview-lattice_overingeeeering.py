#!/usr/bin/env python3

import argparse
import itertools
import string
import sys
from itertools import chain, combinations
from typing import List, Optional, Set, Union, overload

from graphviz import Digraph  # type: ignore

print("Python version")
print(sys.version)
print("Version info.")
print(sys.version_info)


@overload
def get_powerset(l: List[str]) -> List[Set[str]]:
    ...


@overload
def get_powerset(l: List[int]) -> List[Set[int]]:
    ...


def get_powerset(l):
    # type:(Union[List[int], List[str]])->Union[List[Set[str]], List[Set[int]]]
    """ Compute powerset of a set

    Argument:
    ----------
    - l : a list-like of n element with which to construct the powersets
    Returns :
    ----------
    a list of a powerset
    contains 2^n element.

    Example :
    ---------
    >>> get_powerset(["a", "b", "c"])
    [set(),
     {'a'},
     {'b'},
     {'c'},
     {'a', 'b'},
     {'a', 'c'},
     {'b', 'c'},
     {'a', 'b', 'c'}]
    """
    # inpired from recipe in itertool documentation :
    # https://docs.python.org/2/library/itertools.html
    # powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    chained = chain.from_iterable(combinations(l, r)
                                  for r in range(len(l) + 1))
    powersets = [set(e) for e in chained]
    assert(len(powersets) == 2**len(l))
    return powersets


def is_contradictive(s: Set[str]) -> bool:
    """ Say if a set is contradictive.
    == contains both a litteral and its opposite
    Positive literal are represented by upper case letter
    Negative litteral are represented by lower case letter

    Argument :
    -----------
    s : a set of string (litterals are represented by a letter)
    Returns :
    ----------
    boolean is the given set is contradictive

    Examples :
    -----------
    >>> is_contradictive(set(["a", "A"]))
    True
    >>> is_contradictive(set(["a", "b"]))
    False
    """
    for e in s:
        if (e.upper() in s and e.lower() in s):
            return True
    return False


def add_inclusion_link(dot: object,
                       powersets: Union[List[Set[str]], List[Set[int]]]
                       ) -> object:
    """
    Arguments :
    ------------
    dot object
    powersets : list of powersets
    """
    # ensure powersets are sorted by length !
    for si_idx, si in enumerate(sorted(powersets)):
        lensj = None
        for sj in powersets[si_idx:]:
            if (len(si) < len(sj) and si.issubset(sj)):
                # we just found an inclusion.
                # check that we did not already found a smaller inclusion.
                # if yes, quit
                if (lensj is not None and lensj < len(sj)):
                    # once a nope has been found, the loop will not continue
                    # and tries directly the next si. ;)
                    break
                else:
                    lensj = len(sj)
                    dot.edge(str(si), str(sj))
    return dot


def lattice2dot(
        path: str,
        ps: Union[List[Set[str]], List[Set[int]]],
        colored_ps: Optional[Union[List[Set[str]], List[Set[int]]]] = []
) -> object:
    """
    Arguments :
    ------------
    path - str : path where to save image
    ps : list of all the powersets of the lattice
    colored_ps (optional): list of powerset to color
    """

    # create dot image :
    dot = Digraph(name="out", filename=path, format="gv")
    dot.graph_attr.update(ordering='out')

    for e in ps:
        dot.node(str(e), color="green" if e in colored_ps else "black")

    # if colored_ps:
    #     dot.attr('node', color='green')
    #     for e in colored_ps:
    #         dot.node(str(e))
    #
    # remaining_ps = [x for x in ps if x not in colored_ps]
    #
    # dot.attr('node', color='black')
    # for e in remaining_ps:
    #     dot.node(str(e))

    dot = add_inclusion_link(dot, ps)
    return dot


def bn2latticeimg(n: int = 3, p: Optional[str] = "bim.dot") -> object:
    """ Transform a Boolean network of n node to a lattice img

    Arguments :
    ----------
    n - interger : number of node in the BN
    p - str : path where to save image

    Returns :
    --------
    a graphviz digraph object
    """
    atoms = list(itertools.chain(
        *[(string.ascii_uppercase[i], string.ascii_lowercase[i])
          for i in range(0, n)]))
    ps = get_powerset(atoms)
    len(ps)
    # remove contradictive :
    ps = [s for s in ps if not is_contradictive(s)]
    len(ps)
    return lattice2dot(p, ps)


def example_session() -> None:

    powersets = get_powerset(["A", "a", "B", "b", "C", "c"])
    powersets

    contradictive_sets = [is_contradictive(s) for s in powersets]
    powersets = [s for s in powersets if not is_contradictive(s)]

    powersets_found = powersets[0: 3]
    powersets_remaining = [x for x in powersets if x not in powersets_found]
    # cannot use list(set(powersets) - set(powersets_found))
    # because sets are not hashable type ? oO
    # create dot image :
    dot = Digraph(comment='pouloulou', filename="now", format="gv")
    dot.graph_attr.update(ordering='out')
    for e in powersets:
        dot.node(str(e))

    dot.attr('node', color='green')
    for e in powersets_found:
        dot.node(str(e))

    dot.attr('node', color='black')
    for e in powersets_remaining:
        dot.node(str(e))

    print(dot.source)

    # ensure powersets are sorted by length !
    for si_idx, si in enumerate(sorted(powersets)):
        lensj = None
        for sj in powersets[si_idx:]:
            if (len(si) < len(sj) and si.issubset(sj)):
                # we just found an inclusion.
                # check that we did not already found a smaller inclusion.
                # if yes, quit
                if (lensj is not None and
                        lensj < len(sj)):
                    # once a nope has been found, the loop will not continue
                    # and tries directly the next si. ;)
                    break
                else:
                    lensj = len(sj)
                    print(f"'{si}' -> '{sj}'")
                    dot.edge(str(si), str(sj))

    # dot.render()
    dot


#d = bn2latticeimg(4, "BN-4-nodes")
# d.view()

# d
#p = d.render(filename="BN-4-nodes", view=False, cleanup=True)
# print(p)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='boolean network lattice vizualization')
    parser.add_argument('integers', metavar='N', type=int, nargs='+',
                        help='an integer for the accumulator')
    parser.add_argument('--sum', dest='accumulate', action='store_const',
                        const=sum, default=max,
                        help='sum the integers (default: find the max)')

    args = parser.parse_args()
    print(args.accumulate(args.integers))
