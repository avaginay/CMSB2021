import argparse
import pathlib
import re
import sys
import time
import urllib
import urllib.request

from bs4 import BeautifulSoup

try:
    sys.path.insert(1, 'code/')
    from reports import Report
    import sbmlparser

except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--sbmlList',
                        help='Path to the file listing the BIOMDS sbml files taken into account',
                        type=str,
                        required=True
                        )
    parser.add_argument('--outfolderpath',
                        help='Path to the folder where the report have to be saved. The images will be it the subfolder "img"',
                        type=str,
                        required=True
                        )

    # sys.argv = ["--sbmlList", "FROM_CLUSTER/20201011_xp_minMAE_minInputs/sbml-list.txt", "--outfolderpath", "report-curation/"]
    # args = parser.parse_args( sys.argv)
    args = parser.parse_args()

    args.outfolderpath = pathlib.Path(args.outfolderpath)
    (args.outfolderpath / "img").mkdir(parents=True, exist_ok=True)

    rep = Report()
    rep.add_title(f"Gathering of the curation img from the Biomodels website ")

    pattern = re.compile("BIOMD\d{10}")

    with open(args.sbmlList, "r") as f:
        for line in f:

            print('----------')

            # each line is the path of a BIOMD file.
            sbml_filepath = line.strip()

            # get the BIOD_id
            result = pattern.search(sbml_filepath)
            BIOMD_id = result.group(0)

            print(BIOMD_id)
            rep.add_markdown(BIOMD_id)

            # query the website and return the html to the variable 'page'

            urlpage = f"https://www.ebi.ac.uk/biomodels/{BIOMD_id}?format=html"
            print(urlpage)

            req = urllib.request.Request(urlpage)
            req.add_header('accept', 'application/xml; charset=utf8')
            # did not really get why, but i need to add "?format=html" at the end to get it work.
            # also, need to modify the header... oO
            # https://www.ebi.ac.uk/biomodels/docs/#/Model-related%20operations/get__modelId_

            # keep retrying with a delay of 1 sec if meeting an error 500
            # https://stackoverflow.com/a/9986206
            while True:
                try:
                    page = urllib.request.urlopen(req)
                    break
                except urllib.error.HTTPError as detail:
                    if detail.errno == 500:
                        time.sleep(1)
                        print("Has met error 500? Retrying...")
                        continue
                else:
                    raise

            # parse the html using beautiful soup and store in variable 'soup'
            # Avoid error " object of type 'Response' has no len() " when BeautifulSoup the page by first getting text instead of bytes
            # https://stackoverflow.com/a/36709275
            soup = BeautifulSoup(page, 'html.parser')

            # find the image withing the curation div :
            div_Curation = soup.find(id='Curation')

            if div_Curation == None:
                print(
                    f"no divCuration for {BIOMD_id}. Check yourself here : {urlpage}")
                rep.add_markdown(
                    f"no divCuration for {BIOMD_id}. Check yourself here : {urlpage}")
                continue  # next iteration

            # base 64 jpg image to file :

            img_to_retrieve = div_Curation.find_next("img")['src']
            print(img_to_retrieve[0:30])

            filename = f"{BIOMD_id}_curationimg.jpeg"
            print(filename)

            pathname_full = f"{args.outfolderpath}/img/{filename}"
            urllib.request.urlretrieve(
                img_to_retrieve, filename=pathname_full)

            pathname_forreport = f"img/{filename}"
            print(f"pathname_forreport = {pathname_forreport}")
            rep.add_external_figure(pathname_forreport)

            # ----------
            # time unit from the sbml file
            m = sbmlparser.sbmlfile2libsbmlmodel(sbml_filepath)
            units = m.getTimeUnits()
            unitDefinitions = m.getUnitDefinition(units)
            rep.add_markdown(
                f"The time unit extracted from the sbml is : {units}, {unitDefinitions}")
            if not m.isSetTimeUnits():
                rep.add_markdown("**The time unit was not set in the sbml**")

    print("over")
    rep.write_report(
        filename=args.outfolderpath / f'report_curation-from-BIOMODELS.html')
