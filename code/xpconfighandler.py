import yaml


def xpconfigfile2dict(path):
    """
    Parse xpconfig.yaml into dict.

    Argument:
    --------
    - path (str):  path to the xpconfig yaml file

    Returns:
    --------
    xpconfig as a dictionary.
    """
    with open(path, 'r') as stream:
        try:
            xpconfig = yaml.load(stream, Loader=yaml.FullLoader)
            # https://github.com/yaml/pyyaml/wiki/PyYAML-yaml.load(input)-Deprecation
        except yaml.YAMLError as exc:
            print(exc)
    return xpconfig


def xpconfigfileisvalid(path):
    """
    Tells if a xpconfig file is valid

    Argument:
    ---------
    - path (str): path to the xpconfig yaml file

    Return:
    -------
    boolean.
    True is the file contains all the necessary to be a valid xpconfig file.
    """
    xpconfig_dict = xpconfigfile2dict(path)
    return xpconfigisvalid(xpconfig_dict)


def xpconfigisvalid(xpconfig_dict):
    """
    Tells if a given xpconfig is valid

    Argument:
    ---------
    xpconfig_dict (dict): xpconfig stored in a dict.

    Returns:
    --------
    boolean.
    True is the given dict contains all the necessary to be a valid xpconfig.
    """
    required = (
        'clingo_nbthreads',
        'file_sbmlList',
        'folder_bn',
        'folder_heatmapsCompflictingTimeSteps',
        'folder_lp',
        'folder_pkn',
        'folder_sbml',
        'folder_taskdescr',
        'folder_ts',
        'log_level',
        'nodesTypes',
        'optimization',
        'sm_cores',
        'sm_statfile',
        'xp_name'
    )
    return all([k in xpconfig_dict for k in required])
