import sys

try:
    sys.path.insert(1, 'code/')
    import sbmlparser
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


if __name__ == '__main__':

    print("""
    Usage :
        python3 IG-from-SBML_inspired-by-Fages2008.py  BIOMD0000000111_url.xml
    """)

    # ----------
    # %pwd

    # ----------
    # Retrieve arguments :
    assert(len(sys.argv) == 2)  # script name, sbmlfilepath

    sbml_fname = sys.argv[1]
    # sbml_fname = "BIOMD0000000111_url.xml"

    outsif_fpath = sbml_fname.rsplit('.')[0] + "_sif-agent-only.sif"
    print("sif file (agent only) ouputed in '{}'".format(outsif_fpath))
    # ----------
    model = sbmlparser.sbmlfile2libsbmlmodel(sbml_fname)

    len(model.getListOfFunctionDefinitions())
    len(model.getListOfRules())
    len(model.getListOfReactions())
    len(model.getListOfEvents())
    len(model.getListOfSpecies())

    for e in model.getListOfSpecies():
        print(e.getName())

    # nodeA < relationship type : +1 ou -1 > nodeB
    influences = list()

    # iterate over reaction
    # reaction = model.getReaction(1)
    for reaction in model.getListOfReactions():
        print(
            "----------\nr = ",
            reaction,
            reaction.getName(),
            reaction.getKineticLaw().getFormula()
        )
        print(reaction.getKineticLaw().getMath())

        # direction 1
        reactants = {r.getSpecies(): r.getStoichiometry()
                     for r in reaction.getListOfReactants()}
        products = {p.getSpecies(): p.getStoichiometry()
                    for p in reaction.getListOfProducts()}
        modifiers = {m.getSpecies(): m.getSBOTermID()
                     for m in reaction.getListOfModifiers()}

        influences.extend(sbmlparser.relations_from_stoichiometry(
            reactants, products, modifiers))

        # direction 2 :
        if(reaction.getReversible()):
            # /!\ reactant are now reaction.products and vise versa. modifiers stays the same TODO : cheatsheet (je l'ai faite sur papier, for now)
            influences.extend(sbmlparser.relations_from_stoichiometry(
                products, reactants, modifiers))

    # ----------
    # output the SIF :
    print("output the SIF")
    print(influences)
    with open(outsif_fpath, 'w') as f:
        for influence in influences:
            to_write = ' '.join(map(str, influence))
            print(to_write)
            f.write(to_write + '\n')
