import hashlib
import os
import sys

import libsbml
import pytest

import COPASI

try:
    sys.path.insert(1, 'code/')
    # runsimu = __import__('runsimu')
    import runsimu
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


def test_onlyoneargrequired():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        runsimu.main(["a", "b"])
        assert pytest_wrapped_e.type == SystemExit
        #assert pytest_wrapped_e.value.code == 42


def md5(fname):
    """
    Generating an MD5 checksum of a file

    Argument :
    ---------
     fname
    Returns :
    ---------
     hex string representation for the digest
    Note :
    -------
    if not able to fit the whole file in memory, chunks of 4096 bytes are read sequentially and feed the md5 method.

    Example :
    ----------

    >>> md5("data/test/BIOMD0000000111_res-simu_waited.csv")
    '15cb06375be34bfe5b01fda5ab602e07'
    """
    # from https://stackoverflow.com/a/3431838
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


@pytest.fixture
def example_output():
    """ Data test for BIOMD0000000111
    """

    simuoutput_fpath = "data/test/BIOMD0000000111_res-simu.csv"
    if os.path.exists(simuoutput_fpath):
        os.remove(simuoutput_fpath)
    else:
        print("The file did not already exist")

    runsimu.main(["data/test/BIOMD0000000111.xml"])
    return simuoutput_fpath


def test_agents_matching():
    """
    Assert the nodes selected while setting the simulation report in COPASI are the ones corresponding to sbml species + parameters depending on rules.
    (thus, it does not take into account varying sbml parameters changed only by events : https://groups.google.com/forum/#!topic/copasi-user-forum/LdNFeByFqGc)
    """
    sbmlfilename = "data/test/BIOMD0000000111.xml"

    # ----------
    agents_copasi = set()

    dataModel = COPASI.CRootContainer.addDatamodel()
    try:
        # load the model
        if not dataModel.importSBML(sbmlfilename):
            print("Couldn't load {0}:".format(sbmlfilename))
            print(COPASI.CCopasiMessage.getAllMessageText())
    except CCopasiException:
        sys.stderr.write(
            "Error while importing the model from file named \"" + sbmlfilename + "\".\n")
    copasi_model = dataModel.getModel()

    species_taken_copasi = set()
    for i in range(0, copasi_model.getMetabolites().size()):
        metab = copasi_model.getMetabolite(i)
        assert metab is not None
        if metab.getStatus() != COPASI.CModelEntity.Status_FIXED:
            species_taken_copasi.add(metab.getSBMLId())
    len(species_taken_copasi)

    parameters_taken_copasi = set()
    for i in range(0, copasi_model.getModelValues().size()):
        val = copasi_model.getModelValue(i)
        assert val is not None
        if val.getStatus() != COPASI.CModelEntity.Status_FIXED:
            parameters_taken_copasi.add(val.getSBMLId())
    len(parameters_taken_copasi)

    agents_copasi = agents_copasi.union(
        species_taken_copasi, parameters_taken_copasi)
    agents_copasi
    len(agents_copasi)

    # ----------
    agents_sbml = set()

    reader = libsbml.SBMLReader()
    document = reader.readSBML(sbmlfilename)
    if document.getNumErrors() > 0:
        print('readSBML encountered errors while reading the file.')
        document.printErrors()
        sys.exit(1)
    libsbml_model = document.getModel()

    species_taken_sbml = {specie.id for specie in libsbml_model.getListOfSpecies(
    ) if not specie.getConstant()}
    species_taken_sbml
    len(species_taken_sbml)

    parameters_taken_sbml = {rule.getVariable()
                             for rule in libsbml_model.getListOfRules()}
    parameters_taken_sbml
    len(parameters_taken_sbml)

    agents_sbml = agents_sbml.union(species_taken_sbml, parameters_taken_sbml)
    len(agents_sbml)

    # ----------
    assert agents_copasi == agents_sbml


def test_simulationoutput(example_output):
    waited_output = "data/test/BIOMD0000000111_res-simu_waited.csv"

    # At best, they are equal
    if md5(example_output) == md5(waited_output):
        return True

    # Otherwise, just test they have the same header
    else:
        print("csv are not exactly the same")
        with open(example_output) as fex, open(waited_output) as fwa:
            assert (fex.readline() == fwa.readline())
            print("But they have at least the same header")
