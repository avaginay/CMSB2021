
import itertools
import logging
import re
import sys
from itertools import chain

import libsbml

import sympy

logger = logging.getLogger('logger.sbmlparseur')


def get_safeNodeID(nodeID, prefix=""):
    """
    Returns a safe version of a node.
    Especially for LP programs, nodeID are used as atom, thus have to start with a lowercase letter

    Prefix is is lowercased and added with un underscore
    then the nodeID is let as given
    Assert the safe nodeID contains only letters / number / underscore
    (no spaces, no other special char)

    Argument:
    ---------
    -nodeID : string.
        ID or a node
    - prefix : string optional default ""
        A string to be added in front of the nodeID
        It can for example be usefull to identify SBML species and SBML params
        which would have have the same safeID otherwise
    Returns:
    --------
    the safe version of the given nodeID
    """

    try:
        assert(re.match("^[A-Za-z0-9_]*$", nodeID))
    except:
        logger.error(
            f"the node id |{nodeID}| is not only composed of alphanum char and _")
        exit(1)

    # TODO :
    # other traetments for nodeID ?

    if prefix:
        assert(re.match("^[A-Za-z0-9_]*$", prefix))
        return str(prefix).lower() + "_" + str(nodeID)
    else:
        return nodeID


def pairwise_disjoint_nodesTypes(model, nodesTypes):
    """
    Tells if the given nodesTypes are paiswise disjoint.

    Arguments:
    ----------
    - model : libsbml model such as the one get by running
        sbmlfile2libsbmlmodel(sbmlfilepath)
    - nodesTypes (list of string) : the types of node to retrieve.
        For more details about this in the documentation of getListOfNodes funtion.

    Returns:
    --------
    boolean telling whether the given nodesTypes correspond to paiswise disjoint list of nodes.
    For example : SBMLspecies & SBMLparamsnotconstant is ok for sure.
    But SBMLspecies and SBMLleftsideRules might contain the same IDs
    since some SBML species could also happen to be modified by SBML Rules

    TODO :
    ------
    check in SBML documentation if nodes ID are unique
    even in the following case :
    if the same biological entities is present in 2 different compartment,
    can the 2 sbml species have the same ID ?
    I think that this should never be wrong but better safe than sorry...

    """

    nodesTaken = []
    for nodeType in nodesTypes:
        nodesTaken.append(getListOfNodes(model, [nodeType]))
    intersections = [set(i[0]) & set(i[1])
                     for i in itertools.combinations(nodesTaken, 2)]
    logger.debug(intersections)
    return (all([not intersection for intersection in intersections]))


def getListOfNodes(model, nodesTypes, safe=False):
    """
    Retrive list of nodes of a given nodeType from a given model.

    Arguments:
    ----------
    - model : libsbml model such as the one get by running
        sbmlfile2libsbmlmodel(sbmlfilepath)
    - nodesTypes (list of string) : the types of node to retrieve.
        For possible nodeType are for now :
        - `SBMLspecies`. All the species retrieved by libsbml.Model.getListOfSpecies()
        - `SBMLspeciesconstant`. All the constant species retrived by libsbml.Model.getListOfSpecies()
            which have their `constant` attribute set to True
        - `SBMLspeciesnotconstant`. All the constant species retrived by libsbml.Model.getListOfSpecies()
            which have their `constant` attribute set to False
        /!\ It is not the same than COPASI metabolites that are not FIXED
        see here : https://groups.google.com/g/copasi-user-forum/c/LdNFeByFqGc
        for more explanations
        - `SBMLparams` All the params retrieved by libsbml.Model.getListOfParameters()
        - `SBMLparamsnotconstant` All the params that have their value changed by a SBML Rule
        - `SBMLleftsideRules` Left-hand side side of SBML rules (if any). Retrieved by libsbml.Model.Rule.getVariable()
            Note : among the 3 kind of rules, only  AssignmentRule and RateRule have a left-hand side.
            AlgebraicRule has no 'variable', and the call of the function on it will always return an empty string.


    - safe (boolean, default False) whether to return the original ids or a safe version of them
        (through the sbmlparser.get_safeNodeID() function)

    Returns :
    ----------
    list of string corresponding to the id of the elements if the given model belonging to the given nodesType.

    TODO :
    ------
    Take more nodesType into account.
        - foxed / not fixed compartments
        - fixed / not fixed params
            Note : in SBML Level 2, parameter definitions within Reaction structures should not have their 'constant' attribute set to False.
            http://sbml.org/Software/libSBML/5.15.0/docs/python-api/classlibsbml_1_1_parameter.html


    """

    switcher = {
        "SBMLspecies": (lambda model: {specie.id for specie in model.getListOfSpecies()}),
        "SBMLspeciesconstant": (lambda model: {specie.id for specie in model.getListOfSpecies() if specie.get_constant}),
        "SBMLspeciesnotconstant": (lambda model: {specie.id for specie in model.getListOfSpecies() if not specie.get_constant}),
        "SBMLparams": (lambda model: {param.id for param in model.getListOfParameters()}),
        "SBMLparamsnotconstant": (lambda model: {param.id for param in model.getListOfParameters() if param.id in switcher["SBMLleftsideRules"](model)}),
        "SBMLleftsideRules": (lambda model: {rule.getVariable() for rule in model.getListOfRules() if not rule.getVariable() == ""})
        #   Among the 3 kind of rules, only  AssignmentRule and RateRule have a left-hand side.
        #   AlgebraicRule has no 'variable', and the call of the function on it will always return an empty string.
        #   Thus we filter it
    }

    assert(all([nodesType in switcher for nodesType in nodesTypes]))

    if safe:
        l = []
        for nodesType in nodesTypes:
            l.extend(map(get_safeNodeID, switcher[nodesType](
                model), itertools.repeat(nodesType)))
        return l
    else:
        return list(itertools.chain.from_iterable(
            [switcher[nodesType](model) for nodesType in nodesTypes]))


def haserrorwhileopening(sbmlfilepath):
    """
    Tells if an sbml file has error during its opening wit libsbml

    Argument :
    ----------
    sbmlfilepath (str) : path the the sbml file to open

    Return :
    --------
    boolean : True if encountered error / False otherwise
    """
    reader = libsbml.SBMLReader()
    document = reader.readSBML(sbmlfilepath)

    if document.getNumErrors() > 0:
        return True
    return False


def sbmlfile2libsbmlmodel(sbmlfilepath):
    """
    Open an sbml file with the libsbml

    Argument :
    ----------
    sbmlfilepath (str) : path the the sbml file to open
    Return :
    --------
    model object from libsbml

    Print errors encountered while reading the file.
    Still, the model extracted from the file will be returned.
    """
    reader = libsbml.SBMLReader()
    document = reader.readSBML(sbmlfilepath)

    document.getNumErrors()
    if document.getNumErrors() > 0:
        print('readSBML encountered errors while reading the file.')
        document.printErrors()

    # TODO : add a try except rather than assuming model extraction will be all right despite errors were encountered while reading the SBML file

    model = document.getModel()
    return model


def libsbmlmodel2stats(sbmlmodel):
    pass


def sbmlfile2stats(sbmlfilepath):
    pass


def MathML2sympy(mathml):
    """

    - use sympy directly on the string resulting from rule.getFormula() call

    - use mathdom to get a string that can be used in sympy

    - https://groups.google.com/d/mAAsg/sympy/jjPETXzoryA/OmbLHCeRaSYJ
    use xml2dict python library to parse the
    openmath xml and then write your own small routine for traversing the
    dictionary and transforming it into a sympy expression.



    """
    pass


def get_influences_from_sbml_reactions(model, listOfSpeciesTaken, listOfParamsTaken, sanitize=False):
    """

    ----------
    /!\ listOfParamsTaken can theoretically be a list of SBML params.
    BUT if sanitize = True
    the prefix that will be used for the param will in fact be "SBMLparamsnotconstant"
    TODO : fix that
    """
    listOfSpeciesTaken = set(listOfSpeciesTaken)
    listOfParamsTaken = set(listOfParamsTaken)

    influences = list()

    # iterate over reaction
    # reaction = model.getReaction(1)
    for reaction in model.getListOfReactions():
        logger.debug(
            "----------\nr = ",
            reaction,
            reaction.getName(),
            reaction.getKineticLaw().getFormula()
        )
        logger.debug(reaction.getKineticLaw().getMath())

        # direction 1
        reactants = {r.getSpecies(): r.getStoichiometry()
                     for r in reaction.getListOfReactants()}
        products = {p.getSpecies(): p.getStoichiometry()
                    for p in reaction.getListOfProducts()}
        modifiers = {m.getSpecies(): m.getSBOTermID()
                     for m in reaction.getListOfModifiers()}

        logger.debug("reactants", reactants)
        logger.debug("products", products)
        logger.debug("modifiers", modifiers)

        influences.extend(relations_from_stoichiometry(
            reactants, products, modifiers, sanitize=sanitize))

        # direction 2 :
        if(reaction.getReversible()):
            # /!\ reactant are now reaction.products and vise versa. modifiers stays the same TODO : cheatsheet (je l'ai faite sur papier, for now)
            influences.extend(relations_from_stoichiometry(
                products, reactants, modifiers, sanitize=sanitize))

        # other parents : (not part of species)

        logger.debug(reaction.getKineticLaw().getFormula())
        mathNodes = reaction.getKineticLaw().getMath().getListOfNodes()
        for i in range(0, mathNodes.getSize()):
            logger.debug(mathNodes.get(i))
            logger.debug(mathNodes.get(i).getId())
            logger.debug("operator : ", mathNodes.get(i).isOperator())
            logger.debug(mathNodes.get(i).getName())
            logger.debug("in list : ", mathNodes.get(
                i).getName() in listOfParamsTaken)

            if not mathNodes.get(i).isOperator() and mathNodes.get(i).getName() in listOfParamsTaken:
                # since we can not (easily) found the relation sign, we add both;
                # for both product and reactant, but not modifiers since they do not change their stoichiometry :
                logger.debug("add influences with other nodes : ")

                for child in itertools.chain.from_iterable([products.keys(), reactants.keys()]):
                    logger.info(f"{mathNodes.get(i).getName()} ->  {child}")
                    if sanitize:
                        influences.append((
                            get_safeNodeID(mathNodes.get(i).getName(),
                                           "SBMLparamsnotconstant"),
                            1,
                            get_safeNodeID(child, "SBMLspecie")
                        ))
                        influences.append((
                            get_safeNodeID(mathNodes.get(i).getName(),
                                           "SBMLparamsnotconstant"),
                            -1,
                            get_safeNodeID(child, "SBMLspecie")
                        ))
                    else:
                        influences.append(
                            (mathNodes.get(i).getName(), 1, child))
                        influences.append(
                            (mathNodes.get(i).getName(), -1, child))

    return influences


def get_influences_from_sbml_rules(model, listOfSpeciesTaken, listOfParamsTaken, sanitize=False):
    """
    influences extracted from the rules :

    children are left hand side of SBML Rule.
    parents are any node from either listOfSpeciesTaken or listOfParamsTaken

    ----------
    /!\ listOfParamsTaken can theoretically be any list of SBML params.
    BUT if sanitize = True
    the prefix that will be used for the param will in fact be "SBMLparamsnotconstant"
    TODO : fix that
    """
    listOfNodes = list(itertools.chain.from_iterable(
        [listOfSpeciesTaken, listOfParamsTaken]))
    len(listOfNodes)

    influences = list()

    # rule = model.getListOfRules()[2]  # 2 -> MPF
    for rule in model.getListOfRules():
        logger.debug("---------")
        logger.debug(rule)
        logger.debug(rule.getName())
        logger.debug(rule.isAlgebraic())
        logger.debug(rule.isAssignment())
        logger.debug(rule.getVariable())
        child = rule.getVariable()
        logger.debug(rule.toSBML())
        logger.debug(rule.getMath())
        logger.debug(rule.getType())
        logger.debug(rule.getFormula())

        logger.debug(rule.getVariable())

        logger.debug(rule.getFormula())

        if child in listOfNodes:
            sympyrule = sympy.sympify(rule.getFormula())
            logger.debug(type(sympyrule))
            logger.debug(type(sympyrule).__bases__)
            logger.debug(type(sympyrule.free_symbols))

            logger.debug(sympyrule)

            # parent = list(listOfNodes)[0]  # 'slp1T' (can depend, since it's taken from a set)
            for parent in listOfNodes:
                logger.debug(parent)
                # isinstance(2, sympy.Expr)
                if parent in [str(symbol) for symbol in sympyrule.free_symbols]:
                    # since we can not (easily) found the relation sign, we add both :
                    logger.debug("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                    if sanitize:
                        # determine nodeType of the child :
                        if child in listOfSpeciesTaken:
                            childType = "SBMLspecie"
                        elif child in listOfParamsTaken:
                            childType = "SBMLparamsnotconstant"
                        else:
                            logger.error(
                                f"Could not determine nodeType of {child}")
                            exit(1)
                        # determine nodeType of the child :
                        if parent in listOfSpeciesTaken:
                            parentType = "SBMLspecie"
                        elif parent in listOfParamsTaken:
                            parentType = "SBMLparamsnotconstant"
                        else:
                            logger.error(
                                f"Could not determine nodeType of {parent}")
                            exit(1)
                        influences.append((
                            get_safeNodeID(parent, parentType),
                            1,
                            get_safeNodeID(child, childType)
                        ))
                        influences.append((
                            get_safeNodeID(parent, parentType),
                            -1,
                            get_safeNodeID(child, parentType)
                        ))
                    else:
                        influences.append((parent, 1, child))
                        influences.append((parent, -1, child))

    return influences


def relations_from_stoichiometry(reactants, products, modifiers, sanitize=False):
    """
    Returns:
    --------
    influences: a list of 3-elements-tuples that describe the label edges :
        - node X
        - influence type (1 : activation or -1 : inhibition)
        - node Y
    """
    influences = list()

    # known activateur (SBO:0000459 = "stimulator" or SBO:0000595 = "dual activity modifier") :
    activators = {m: SBO for m, SBO in modifiers.items() if (
        SBO == "SBO:0000459" or SBO == "SBO:0000595")}
    # known inhibitor (SBO:0000020 = "inhibitor" or SBO:0000595 = "dual activity modifier")
    inhibitors = {m: SBO for m, SBO in modifiers.items() if (
        SBO == "SBO:0000020" or SBO == "SBO:0000595")}
    # unknown (no SBO):
    unknown = {m: SBO for m, SBO in modifiers.items() if (
        SBO not in ("SBO:0000459", "SBO:0000020"))}

    # other SBO exist
    # for example : SBO:0000460 = enzymatic catalist

    # iterate over Y (reactant or product)
    # (but not modifiers
    #   - because no relation X -> Y will be derived from this reaction if Y is a modifier
    #   - because a modifier does not have a stoichiometry anyway...)
    for y in chain(reactants.keys(), products.keys()):
        print(y)
    for y in chain(reactants.keys(), products.keys()):

        # compute net stoichiometry:
        net_stoichiometry = reactants.get(y, 0) - products.get(y, 0)
        # (use 0 if non determined)

        # what to do if equal 0 ?
        if net_stoichiometry == 0:
            logger.warning(
                "The stoichiometry did not changed, which is weird ! :O")

        # iterate over X (reactif of modifiers
        for x in chain(reactants.keys(), modifiers.keys()):
            print(x)
        for x in chain(reactants.keys(), modifiers.keys()):

            flag_case = False
            # evaluate the sign of the net_stoichiometry and the role of X
            # determine the edge X -> Y according to the rules. (see Fages 2008)

            if (net_stoichiometry >= 0):

                # unknown modifiers are considered both as activaors and inhibutors.
                # reactants and activators behave the same :
                if(x in chain(reactants.keys(), activators.keys(), unknown.keys())):
                    print(x, -1, y)
                    if sanitize:
                        influences.append((
                            get_safeNodeID(x, "SBMLspecies"),
                            -1,
                            get_safeNodeID(y, "SBMLspecies")
                        ))
                    else:
                        influences.append((x, -1, y))
                    flag_case = True

                if(x in chain(inhibitors.keys(), unknown.keys())):
                    print(x, 1, y)
                    if sanitize:
                        influences.append((
                            get_safeNodeID(x, "SBMLspecies"),
                            1,
                            get_safeNodeID(y, "SBMLspecies")
                        ))
                    else:
                        influences.append((x, 1, y))
                    flag_case = True

                if not flag_case:
                    logger.error("----------")
                    logger.error(f"x : {x}")
                    logger.error(f"y : {y}")

                    logger.error(f"reactants : {reactants}")
                    logger.error(f"products : {products}")
                    logger.error(f"modifiers : {modifiers}")
                    logger.error(f"activators : {activators}")
                    logger.error(f"inhibitors : {inhibitors}")
                    logger.error(f"unknown : {unknown}")

                    logger.error(f"net_stoichiometry: {net_stoichiometry}")
                    for kind_name, kind_value in {
                        "reactants": reactants.keys(),
                        "activators": activators.keys(),
                        "inhibitors": inhibitors.keys(),
                        "unknown": unknown.keys()
                    }.items():
                        print(f"x in {kind_name} : {x in kind_value}")

                    logger.error("case untreaded. Problem")
                    exit(1)

            elif(net_stoichiometry <= 0):
                # reactant - product < 0  ->  y has been produced

                # reactants and activators behave the same :
                if(x in chain(reactants.keys(), activators.keys(), unknown.keys())):
                    print(x, 1, y)
                    if sanitize:
                        influences.append((
                            get_safeNodeID(x, "SBMLspecies"),
                            1,
                            get_safeNodeID(y, "SBMLspecies")
                        ))
                    else:
                        influences.append((x, 1, y))
                    flag_case = True

                if(x in chain(inhibitors.keys(), unknown.keys())):
                    print(x, -1, y)
                    if sanitize:
                        influences.append((
                            get_safeNodeID(x, "SBMLspecies"),
                            -1,
                            get_safeNodeID(y, "SBMLspecies")
                        ))
                    else:
                        influences.append((x, -1, y))
                    flag_case = True

                if not flag_case:
                    logger.error("----------")
                    logger.error(f"x : {x}")
                    logger.error(f"y : {y}")

                    logger.error(f"reactants : {reactants}")
                    logger.error(f"products : {products}")
                    logger.error(f"modifiers : {modifiers}")
                    logger.error(f"activators : {activators}")
                    logger.error(f"inhibitors : {inhibitors}")
                    logger.error(f"unknown : {unknown}")

                    logger.error(f"net_stoichiometry: {net_stoichiometry}")
                    for kind_name, kind_value in {
                        "reactants": reactants.keys(),
                        "activators": activators.keys(),
                        "inhibitors": inhibitors.keys(),
                        "unknown": unknown.keys()
                    }.items():
                        print(f"x in {kind_name} : {x in kind_value}")

                    logger.error("case untreaded. Problem")
                    exit(1)
    return influences


def get_all_influences(model, sanitize=False):
    """
    Extract all the influences that I know how to extract.
    /!\ They only take into account node of the folowing nodeType :
    SBMLspecies and SBMLparamsnotconstant

    Arguments:
    ----------
    - model (libsbml.Model instance)
    - sanitize (boolean default False)
        if True, it uses safe nodeIDs insted of the original IDs
        thus, "SBMLspecies" or "SBMLparamsnotconstant" will entre autre be prefixed to the nodes

    Returns:
    ---------
    sif structure containing all the influences.
    The influences concerning SBML species are extracted from SBML Reaction looking at the stoichiometry
    Influences concerning Variable of SBML Rules are overapproximated
        by considering their upstreamNodes as both activators and inhibitors.

    For the details about the extraction of influences, see the LaTeX document descibing the pipeline
    """

    listOfSpeciesTaken = getListOfNodes(model, ["SBMLspecies"])
    listOfParamsTaken = getListOfNodes(model, ["SBMLparamsnotconstant"])

    influences = list()

    # influences from reaction
    influences.extend(get_influences_from_sbml_reactions(
        model, listOfSpeciesTaken, listOfParamsTaken, sanitize=sanitize))
    influences

    # add influences extracted from the rules :
    influences.extend(get_influences_from_sbml_rules(
        model, listOfSpeciesTaken, listOfParamsTaken, sanitize=sanitize))
    influences

    return influences


def get_influences(model, listOfNodesTaken, sanitize=False):
    """
    Extract all the influences uses nodes present in the given list

    Arguments:
    ----------
    - model (libsbml.Model instance)
    - listOfNodesTaken (list of string)
        List of the nodes id the extracted influences have to concern
    - sanitize (boolean default False)
        if True, it uses safe nodeIDs insted of the original IDs


    Returns:
    ---------
    sif structure containing all the influences that have been found councerning the nodes from the provided listOfNodesTaken
    """

    # Retrieve all the influence you can ....
    influences_all = get_all_influences(model, sanitize=sanitize)

    # ... then restrict the influences to the ones implying nodes from the provided listOfNodesTaken
    # -> remove influences that use agents that are not in listOfNodesTaken
    influences_filtered = [(from_x, val, to_y) for (from_x, val, to_y) in influences_all
                           if from_x in listOfNodesTaken and to_y in listOfNodesTaken]

    return influences_filtered
