import pathlib

for key in (
        "folder_sbml",
        "folder_pkn",
        "folder_ts",
        "folder_bn"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])


gW_SYSTEM_id_ts, = glob_wildcards(
    config['folder_ts'] / "{SYSTEM_id}_ts.csv")
gW_SYSTEM_id_pkn, = glob_wildcards(
    config['folder_pkn'] / "{SYSTEM_id}_pkn.sif")
gW_SYSTEM_id = gW_SYSTEM_id_ts + gW_SYSTEM_id_pkn


gW_SYSTEM_id_generatedbnet, gW_identification_method, gW_BNid_generatedbnet = glob_wildcards(
    config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" / "{BN_id}.bnet")
print("generated bnet:")
print(gW_SYSTEM_id_generatedbnet)
print(gW_identification_method)
print(gW_BNid_generatedbnet)


rule all_get_coverage_stgVSts:
    input:
        expand(
            config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
            f"{{BN_id}}_coverage_stgVSts_{config['coverage_updatescheme']}.csv",
            zip,
            SYSTEM_id=gW_SYSTEM_id_generatedbnet,
            identification_method=gW_identification_method,
            BN_id=gW_BNid_generatedbnet
        )


rule all_bnet2stgsif:
    input:
        # complete stg synchrounous
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-synchronous_complete.sif",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        # complete stg asynchrounous
        # /!\ can be extra big !!:
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-asynchronous_complete.sif",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        # focus stg synchrounous
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-synchronous_focusedbts.sif",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               ),
        # focus stg asynchrounous
        expand(config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
               "{BN_id}_stg-asynchronous_focusedbts.sif",
               zip,
               SYSTEM_id=gW_SYSTEM_id_generatedbnet,
               identification_method=gW_identification_method,
               BN_id=gW_BNid_generatedbnet
               )


rule bnet2stgsif_complete:
    input:
        # Wildcard constraints in inputs are ignored.
        bnet = config["folder_bn"] / "{SYSTEM_id}" / \
            "{identification_method}" / "{BN_id}.bnet",
        binarizedts = config['folder_ts'] / "{SYSTEM_id}_binarizedtsts.csv"
    output:
        config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" /
        "{BN_id}_stg-{updatescheme}_complete.sif"
    shell:
        """
        python3 code/bnet2stgsif.py --updatescheme {wildcards.updatescheme} --bnetfile {input.bnet} --binarizedtscsv {input.binarizedts} --outputfile {output}
        sed -i 's/-1/0/g' {output}
        """


rule bnet2stgsif_focusedbts:
    input:
        # Wildcard constraints in inputs are ignored.
        bnet = config["folder_bn"] / "{SYSTEM_id}" / \
            "{identification_method}" / "{BN_id}.bnet",
        binarizedts = config['folder_ts'] / "{SYSTEM_id}_binarizedtsts.csv"
    params:
        "--focusedbts"
    output:
        config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" / \
            "{BN_id}_stg-{updatescheme}_focusedbts.sif"
    shell:
        """
        python3 code/bnet2stgsif.py --updatescheme {wildcards.updatescheme} --bnetfile {input.bnet} --binarizedtscsv {input.binarizedts} {params} --outputfile {output}
        sed -i 's/-1/0/g' {output}
        """


rule get_coverage_stgVSts:
    input:
        bnet = config["folder_bn"] / "{SYSTEM_id}" / \
            "{identification_method}" / "{BN_id}.bnet",
        binarizedts = config['folder_ts'] / "{SYSTEM_id}_binarizedtsts.csv"
    output:
        config["folder_bn"] / "{SYSTEM_id}" / \
            "{identification_method}" / \
            "{BN_id}_coverage_stgVSts_{coverage_updatescheme}.csv"
    shell:
        "python3 code/coverage_stgVSts.py --bnetfile {input.bnet} --binarizedtscsv {input.binarizedts} --outputfile {output} --updatescheme {wildcards.coverage_updatescheme}"


print(expand(
    config["folder_bn"] / "{SYSTEM_id}" /
    "{identification_method}" /
    "coverage-{measure}-{updatescheme}-boxplot_stgVSts.png",
    SYSTEM_id=set(gW_SYSTEM_id_generatedbnet),
    identification_method=set(gW_identification_method),
    measure=["retreived_edges__number", "retreived_edges__proportion"],
    updatescheme=["asynchronous"]
))
